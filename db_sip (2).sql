-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2020 at 11:05 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(15) NOT NULL,
  `name_kelas` varchar(20) NOT NULL,
  `guru_id` int(15) NOT NULL,
  `tingkat` varchar(20) NOT NULL,
  `jurusan` varchar(20) NOT NULL,
  `is_active` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `name_kelas`, `guru_id`, `tingkat`, `jurusan`, `is_active`) VALUES
(1, '10 IPS A', 8, 'X', 'IPA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(15) NOT NULL,
  `nama_mapel` varchar(230) NOT NULL,
  `tingkat` varchar(2) NOT NULL,
  `kd_mapel` varchar(10) NOT NULL,
  `jurusan` varchar(20) NOT NULL,
  `guru_id` int(15) NOT NULL,
  `is_un` tinyint(1) DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`id_mapel`, `nama_mapel`, `tingkat`, `kd_mapel`, `jurusan`, `guru_id`, `is_un`, `is_active`) VALUES
(1, 'Bahasa Indonesia', 'X', 'KD-MAPEL-2', 'IPA', 8, 1, 1),
(4, 'Matematika', 'X', 'MP-02', 'IPA', 8, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `id_nilai` int(15) NOT NULL,
  `siswa_id` int(15) NOT NULL,
  `mapel_id` int(15) NOT NULL,
  `tipe_id` int(15) NOT NULL,
  `nilai` int(20) NOT NULL,
  `input_id` int(15) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_nilai`
--

INSERT INTO `tb_nilai` (`id_nilai`, `siswa_id`, `mapel_id`, `tipe_id`, `nilai`, `input_id`, `create_date`) VALUES
(2, 7, 1, 2, 80, 1, '2020-06-21 20:05:29'),
(3, 7, 1, 5, 50, 1, '2020-06-21 20:06:07'),
(5, 7, 4, 2, 80, 1, '2020-06-22 22:54:14'),
(6, 7, 4, 5, 90, 1, '2020-06-22 23:22:16'),
(7, 7, 1, 8, 90, 1, '2020-06-26 10:21:49'),
(8, 7, 1, 8, 90, 1, '2020-06-26 10:22:05'),
(9, 7, 1, 8, 80, 1, '2020-06-30 10:32:43'),
(10, 9, 1, 2, 80, 1, '2020-06-30 10:49:30'),
(11, 9, 4, 2, 90, 1, '2020-06-30 10:50:00'),
(12, 7, 1, 8, 90, 8, '2020-06-30 14:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_akhir`
--

CREATE TABLE `tb_nilai_akhir` (
  `id_nilai_akhir` int(15) NOT NULL,
  `siswa_id` int(15) NOT NULL,
  `mapel_id` int(15) NOT NULL,
  `tipe_id` int(15) NOT NULL,
  `nilai_akhir` int(30) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_nilai_akhir`
--

INSERT INTO `tb_nilai_akhir` (`id_nilai_akhir`, `siswa_id`, `mapel_id`, `tipe_id`, `nilai_akhir`, `last_update`) VALUES
(42, 7, 1, 2, 23, '2020-06-30 03:29:26'),
(43, 7, 4, 2, 24, '2020-06-30 03:29:26'),
(44, 7, 1, 5, 15, '2020-06-30 03:29:26'),
(45, 7, 4, 5, 27, '2020-06-30 03:29:26'),
(46, 7, 1, 8, 27, '2020-06-30 03:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_raport`
--

CREATE TABLE `tb_raport` (
  `id_raport` int(11) NOT NULL,
  `siswa_id` int(15) NOT NULL,
  `mapel_id` int(15) NOT NULL,
  `nilai` int(15) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_raport`
--

INSERT INTO `tb_raport` (`id_raport`, `siswa_id`, `mapel_id`, `nilai`, `last_update`) VALUES
(94, 1, 1, 24, '2020-06-24 02:34:53'),
(95, 7, 1, 42, '2020-06-24 02:34:53'),
(96, 7, 4, 51, '2020-06-24 02:34:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tipe_nilai`
--

CREATE TABLE `tb_tipe_nilai` (
  `id_tipe_nilai` int(11) NOT NULL,
  `nama_tipe_nilai` varchar(20) NOT NULL,
  `bobot` int(10) NOT NULL,
  `is_multi_input` tinyint(1) DEFAULT 1,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_tipe_nilai`
--

INSERT INTO `tb_tipe_nilai` (`id_tipe_nilai`, `nama_tipe_nilai`, `bobot`, `is_multi_input`, `deskripsi`) VALUES
(2, 'UAS', 30, 0, 'OKE'),
(5, 'UTS', 30, 0, 'Untuk UTS'),
(8, 'Tugas', 30, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_un`
--

CREATE TABLE `tb_un` (
  `id_un` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `nilai_un` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_un`
--

INSERT INTO `tb_un` (`id_un`, `siswa_id`, `mapel_id`, `nilai_un`) VALUES
(4, 7, 1, 89);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(15) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `state` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `kelas_id` int(15) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `ni` varchar(30) NOT NULL,
  `tpt_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `name_user`, `state`, `username`, `password`, `alamat`, `phone`, `email`, `image`, `kelas_id`, `is_active`, `ni`, `tpt_lahir`, `tgl_lahir`) VALUES
(1, 'Muhamad Khaikal', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Binong', '0895342960174', 'khaikalaz1@gmail.com', 'img_Muhamad_Khaikal_Azharo.JPG', 1, 1, '', '', '0000-00-00'),
(7, 'Ujang Suherman', 'siswa', 'ujang98', '81dc9bdb52d04dc20036dbd8313ed055', 'ujang98', '087879', 'ujang@gmail.com', 'img_Ujang_Suherman.JPG', 1, 1, '990001', 'Subang', '1998-07-09'),
(8, 'Ahmad Sobari', 'guru', 'ahmad', '4297f44b13955235245b2497399d7a93', 'Kp Batang Pohon', '0878998790', 'ahmad@gmail.com', 'img_Ahmad_Sobari.JPG', NULL, 1, '101011', 'Sumedang', '1988-01-13'),
(9, 'Budi Sudarsono', 'siswa', 'Budi', 'e10adc3949ba59abbe56e057f20f883e', 'Oke', '089', 'budi@gmail.com', 'img_Budi_Sudarsono.jpg', 1, 1, '9900990', 'Subang', '2020-06-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `tb_nilai_akhir`
--
ALTER TABLE `tb_nilai_akhir`
  ADD PRIMARY KEY (`id_nilai_akhir`);

--
-- Indexes for table `tb_raport`
--
ALTER TABLE `tb_raport`
  ADD PRIMARY KEY (`id_raport`);

--
-- Indexes for table `tb_tipe_nilai`
--
ALTER TABLE `tb_tipe_nilai`
  ADD PRIMARY KEY (`id_tipe_nilai`);

--
-- Indexes for table `tb_un`
--
ALTER TABLE `tb_un`
  ADD PRIMARY KEY (`id_un`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  MODIFY `id_nilai` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_nilai_akhir`
--
ALTER TABLE `tb_nilai_akhir`
  MODIFY `id_nilai_akhir` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_raport`
--
ALTER TABLE `tb_raport`
  MODIFY `id_raport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `tb_tipe_nilai`
--
ALTER TABLE `tb_tipe_nilai`
  MODIFY `id_tipe_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_un`
--
ALTER TABLE `tb_un`
  MODIFY `id_un` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
