<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiswaNilai extends CI_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see https://codeigniter.com/user_guide/general/urls.html
*/
	public function __construct()
	{
		parent::__construct();

        // Load pdf library
        $this->load->library('dompdf_gen');
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_Nilai');
		$this->load->model('M_NilaiAkhir');
		$this->load->model('M_Tipe_nilai');
		$this->load->model('M_Raport');
		$this->load->model('M_Un');
		if($this->session->userdata('state') != "siswa")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
	}

	public function index()
	{
		$data['nilais'] = $this->M_Nilai->getNilaiBySiswa();
		$data_to_template = array(
			'title' => 'Siswa Nilai',
			'content' => $this->load->view('siswa/siswanilai', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);

	// print_r($data_to_template['title']);
	// die();

		$this->load->view('layouts_front/template', $data_to_template);	
	}

	public function show_nilai()
	{
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_nilais = $this->M_Nilai->getNilaiBySiswa();
		$no = 0;   
		foreach ($data_nilais as $n) {
			$no++;
			$data_table2 .='
			<tr>
			<td>'.$no.'</td>
			<td>'.$n->nama_tipe_nilai.'</td>
			<td>'.$n->nama_mapel.'</td>
			<td>'.$n->nilai.'</td>
			<td>'.$n->input_oleh.'</td>
			</tr>
			';
		}
		$data_table1 .='
		<a class="btn btn-danger" href='.base_url('index.php/siswa/SiswaNilai/cetak_nilai').'>Cetak</a>
		<br><br>
		<table id="example2" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Tipe Nilai</th>
				<th>Mata Pelajaran</th>
				<th>Nilai</th>
				<th>Diinput Oleh</th>
			</tr>
		</thead>
		<tbody>';
		$data_table3 .="</tbody></table>";
		$data_table=$data_table1.''.$data_table2.''.$data_table3 ;
		if ($data_table){
			$code=1;
		}
		echo json_encode(array('data_table' => $data_table,'code' => $code));
	}

	public function show_na()
	{
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_nilais = $this->M_NilaiAkhir->getNaBySiswa();
		$no = 0;   
		foreach ($data_nilais as $na) {
			$no++;
			$data_table2 .='
			<tr>
			<td>'.$no.'</td>
			<td>'.$na->name_mapel.'</td>
            <td>'.$na->nama_tipe_nilai.'</td>
            <td>'.$na->nilai_akhir.'</td>
            <td>'.$na->last_update.'</td>
			</tr>
			';
		}
		$data_table1 .='
		<a class="btn btn-danger" href='.base_url('index.php/siswa/SiswaNilai/cetak_na').'>Cetak</a>
		<br><br>
		<table id="example2" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Mata Pelajaran</th>
                <th>Tipe Penilaian</th>
                <th>Nilai Akhir</th>
                <th>Last Update</th>
			</tr>
		</thead>
		<tbody>';
		$data_table3 .="</tbody></table>";
		$data_table=$data_table1.''.$data_table2.''.$data_table3 ;
		if ($data_table){
			$code=1;
		}
		echo json_encode(array('data_table' => $data_table,'code' => $code));
	}

	public function show_nr()
	{
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_nrs = $this->M_Raport->getRaportBySiswa();
		$no = 0;   
		foreach ($data_nrs as $nr) {
			$no++;
			$data_table2 .='
			<tr>
			<td>'.$no.'</td>
			<td>'.$nr->name_mapel.'</td>
            <td>'.$nr->nilai.'</td>
            <td>'.$nr->last_update.'</td>
			</tr>
			';
		}
		$data_table1 .='
		<a class="btn btn-danger" href='.base_url('index.php/siswa/SiswaNilai/cetak_nr').'>Cetak</a>
		<br><br>
		<table id="example2" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Mata Pelajaran</th>
                <th>Nilai Raport</th>
                <th>Last Update</th>
			</tr>
		</thead>
		<tbody>';
		$data_table3 .="</tbody></table>";
		$data_table=$data_table1.''.$data_table2.''.$data_table3 ;
		if ($data_table){
			$code=1;
		}
		echo json_encode(array('data_table' => $data_table,'code' => $code));
	}

	public function show_un()
	{
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_uns = $this->M_Un->getUnBySiswa();
		$no = 0;   
		foreach ($data_uns as $un) {
			$no++;
			$data_table2 .='
			<tr>
			<td>'.$no.'</td>
            <td>'.$un->nama_mapel.'</td>
            <td>'.$un->nilai_un.'</td>
			</tr>
			';
		}
		$data_table1 .='
		<a class="btn btn-danger" href='.base_url('index.php/siswa/SiswaNilai/cetak_un').'>Cetak</a>
		<br><br>
		<table id="example2" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Mata Pelajaran</th>
				<th>Nilai Ujian Nasional</th>
			</tr>
		</thead>
		<tbody>';
		$data_table3 .="</tbody></table>";
		$data_table=$data_table1.''.$data_table2.''.$data_table3 ;
		if ($data_table){
			$code=1;
		}
		echo json_encode(array('data_table' => $data_table,'code' => $code));
	}

	public function cetak_nilai(){
		$data['siswa'] = $this->M_User->getMe();
		$data['nilais']= $this->M_Nilai->getNilaiBySiswa();
		// lod from view
        $this->load->view("admin/report/nilai_pdf", $data);
        //get output from view
        $html = $this->output->get_output();
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');
        
        // Render the HTML as dompdf
        $this->dompdf->render();
        
        // Output the generated dompdf (1 = download and 0 = preview)
        $this->dompdf->stream("Data Nilai ".$data['siswa']->name_user.".pdf", array("Attachment"=>0));
	}

	public function cetak_na(){
		$data['siswa'] = $this->M_User->getMe();
		$data['nas']= $this->M_NilaiAkhir->getNaBySiswa();
		// lod from view
        $this->load->view("admin/report/na_pdf", $data);
        //get output from view
        $html = $this->output->get_output();
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');
        
        // Render the HTML as dompdf
        $this->dompdf->render();
        
        // Output the generated dompdf (1 = download and 0 = preview)
        $this->dompdf->stream("Data Nilai Akhir".$data['siswa']->name_user.".pdf", array("Attachment"=>0));
	}

	public function cetak_nr(){
		$data['siswa'] = $this->M_User->getMe();
		$data['nrs']= $this->M_Raport->getRaportBySiswa();
		// lod from view
        $this->load->view("admin/report/nr_pdf", $data);
        //get output from view
        $html = $this->output->get_output();
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');
        
        // Render the HTML as dompdf
        $this->dompdf->render();
        
        // Output the generated dompdf (1 = download and 0 = preview)
        $this->dompdf->stream("Data Nilai Raport".$data['siswa']->name_user.".pdf", array("Attachment"=>0));
	}

	public function cetak_un(){
		$data['siswa'] = $this->M_User->getMe();
		$data['uns']= $this->M_Un->getUnBySiswa();
		// lod from view
        $this->load->view("admin/report/un_pdf", $data);
        //get output from view
        $html = $this->output->get_output();
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');
        
        // Render the HTML as dompdf
        $this->dompdf->render();
        
        // Output the generated dompdf (1 = download and 0 = preview)
        $this->dompdf->stream("Data Nilai Ujian Nasionan".$data['siswa']->name_user.".pdf", array("Attachment"=>0));
	}
}
