<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['mapels'] = $this->M_Mapel->getMapel();
		$data['gurus'] = $this->M_User->getGuru();
		$data_to_template = array(
			'title' => 'Data Mata Pelajaran',
			'content' => $this->load->view('admin/mapel', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar_master', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_add()
	{
		$add_mapel = $this->M_Mapel->save();

		if ($add_mapel) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}

		redirect ('admin/Mapel');
	}

	public function do_edit()
	{
		// print_r($this->input->post());
		// die();
		$edit = $this->M_Mapel->update();
		
		if ($edit) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('admin/Mapel');	
	}

	public function delete($id_mapel)
	{
		$delete = $this->M_Mapel->delete($id_mapel);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('admin/Mapel');
	}
	
}
