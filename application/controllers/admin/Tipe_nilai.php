<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_nilai extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_Tipe_nilai');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['mapels'] = $this->M_Mapel->getMapel();
		$data['tipe_nilais'] = $this->M_Tipe_nilai->getAll();
		$data['total_bobot'] = $this->M_Tipe_nilai->totalBobot();
		$data_to_template = array(
			'title' => 'Data Tipe Nilai',
			'content' => $this->load->view('admin/tipe_nilai', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar_master', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_add()
	{
		$cek = $this->M_Tipe_nilai->getBobot();
		if ($cek <= 100){
			$add_tipe_nilai = $this->M_Tipe_nilai->save();

			if ($add_tipe_nilai) {
				$msg ="Data berhasil ditambahkan";
			}else{
				$msg = "Data gagal ditambahkan";
			}
		}else{
			$msg ="Bobot melebihi batas";
		}

		echo json_encode(array('msg' => $msg));
	}

	public function do_edit()
	{
		$cek = $this->M_Tipe_nilai->getBobot();
		if ($cek <= 100){
			$add_tipe_nilai = $this->M_Tipe_nilai->update();

			if ($add_tipe_nilai) {
				$this->session->set_flashdata('success', 'Data berhasil diubah');
			}else{
				$this->session->set_flashdata('error', 'Data gagal diubah');
			}
		}else{
			$this->session->set_flashdata('error', 'Bobot melebihi batas');
		}

		redirect ('admin/Tipe_nilai');
	}

	public function delete($id_tipe_nilai)
	{
		$delete = $this->M_Tipe_nilai->delete($id_tipe_nilai);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('admin/Tipe_nilai');
	}
	
}
