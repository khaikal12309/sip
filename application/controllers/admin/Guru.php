<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['users'] = $this->M_User->getGuru();
		$data_to_template = array(
			'title' => 'Data Guru',
			'content' => $this->load->view('admin/guru', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar_master', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tb_user.email]');
		$this->form_validation->set_rules('ni', 'NIP', 'required|is_unique[tb_user.ni]');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[tb_user.username]');

		if ($this->form_validation->run() == TRUE)
		{
			$add_user = $this->M_User->save_guru();
			if ($add_user) {
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
			}else{
				$this->session->set_flashdata('error', 'Data gagal ditambahkan');
			}
		} else {
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}

		redirect ('admin/Guru');
	}

	public function do_edit()
	{
		$edit = $this->M_User->update();
		
		if ($edit) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('admin/Guru');	
	}

	public function delete($id_user)
	{
		$delete = $this->M_User->delete($id_user);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('admin/Guru');
	}

	public function change_password()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('old_password' , 'Old Password', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', 'Data gagal diubah');
		} 
		else 
		{
			$cek = $this->M_User->cek();
			if ($cek)
			{
				$change_password = $this->M_User->update_pass();
				if ($change_password) 
				{
					$this->session->set_flashdata('success', 'Password berhasil diubah');
				}
				else
				{
					$this->session->set_flashdata('error', 'Password gagal diubah');
				}	
			}
			else
			{
				$this->session->set_flashdata('error', 'Password gagal diubah');
			}
			
		}
		redirect ('admin/Guru');
	}

	public function export_excel()
	{
		$data = array( 'title' => 'Laporan Excel',
			'users' => $this->M_User->getGuru());
		$this->load->view('admin/report/user_excel',$data);
	}

	public function export_pdf($pdf=false)
	{
		$data['users']= $this->M_User->getGuru();
		// lod from view
        $this->load->view("admin/report/user_pdf", $data);
        //get output from view
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('dompdf_gen');
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as dompdf
        $this->dompdf->render();
        
        // Output the generated dompdf (1 = download and 0 = preview)
        $this->dompdf->stream("Report_Dist_.pdf", array("Attachment"=>0));

	}
}
