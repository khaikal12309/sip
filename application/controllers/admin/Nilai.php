<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller 
{

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see https://codeigniter.com/user_guide/general/urls.html
*/
public function __construct()
{
	parent::__construct();
	$this->load->model('M_User');
	$this->load->model('M_Kelas');
	$this->load->model('M_Mapel');
	$this->load->model('M_Nilai');
	$this->load->model('M_Tipe_nilai');
	if($this->session->userdata('state') != "admin")
	{
		$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		redirect("auth");
	}
}

public function index()
{
	$data['nilais'] = $this->M_Nilai->getAll();
// print_r($data);
// die();
	$data['mapels'] = $this->M_Mapel->getMapel();
	$data['siswas'] = $this->M_User->getSiswa();
	$data['tipe_nilais'] = $this->M_Tipe_nilai->getAll();
	$data_to_template = array(
		'title' => 'Data Penilaian',
		'content' => $this->load->view('admin/nilai', $data, TRUE),
		'navbar' => $this->load->view('layouts/navbar', null, TRUE),
		'sidebar' => $this->load->view('layouts/sidebar', null, TRUE),
		'footer' => $this->load->view('layouts/footer', null, TRUE),
	);
	$this->load->view('layouts/template', $data_to_template);	
}

public function do_add()
{
	$id_tipe_nilai= $this->input->post('tipe_id');
	$tipe_nilai = $this->db->query('SELECT * from tb_tipe_nilai where id_tipe_nilai = '.$id_tipe_nilai.' limit 1')->result();
	if ($tipe_nilai[0]->is_multi_input == False){
		$cek = $this->M_Nilai->cek();
		if ($cek->num_rows()==0)
		{
			$add_nilai = $this->M_Nilai->save();
			if ($add_nilai) {
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
			}else{
				$this->session->set_flashdata('error', 'Data gagal ditambahkan');
			}
		}else{
			$data = $cek->result();
			
			$this->session->set_flashdata('id_nilai',$data[0]->id_nilai);
			$this->session->set_flashdata('error', 'Data nilai pada siswa telah tersedia');
		}
	}else{
		$add_nilai = $this->M_Nilai->save();
		if ($add_nilai) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}
	}
	redirect ('admin/Nilai');
}

public function do_edit()
{
	$edit = $this->M_Nilai->update();

	if ($edit) {
		$this->session->set_flashdata('success', 'Data berhasil diubah');
	}else{
		$this->session->set_flashdata('error', 'Data gagal diubah');
	}

	redirect ('admin/Nilai');	
}

public function delete($id_nilai)
{
	$delete = $this->M_Nilai->delete($id_nilai);
	if ($delete){
		$this->session->set_flashdata('success', 'Data berhasil dihapus');
	}else{
		$this->session->set_flashdata('error', 'Data gagal dihapus');
	}
	redirect ('admin/Nilai');
}

}
