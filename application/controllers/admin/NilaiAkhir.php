<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiAkhir extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_NilaiAkhir');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['nilai_akhirs'] = $this->M_NilaiAkhir->getAll();
		$data_to_template = array(
			'title' => 'Data Nilai Akhir',
			'content' => $this->load->view('admin/nilai_akhir', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_update()
	{
		$data_nilai_akhirs = $this->M_NilaiAkhir->getNa();
		$cek = count($this->M_NilaiAkhir->getAll());
		if ($cek != 0){
			$this->M_NilaiAkhir->empty_table();
		}
		foreach($data_nilai_akhirs as $d)
		{
			$this->siswa_id = $d->siswa_id;
			$this->mapel_id = $d->mapel_id;
			$this->tipe_id = $d->tipe_id;
			$this->nilai_akhir = $d->n_a;        
         	$this->db->insert('tb_nilai_akhir', $this);
		}
		redirect ('admin/NilaiAkhir');
	}
}
