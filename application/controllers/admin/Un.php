<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Un extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Un');
		$this->load->model('M_Mapel');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['mapels'] = $this->M_Mapel->getMapelUN();
		$data['siswas'] = $this->M_User->getSiswa();
		$data['uns'] = $this->M_Un->getAll();
		$data_to_template = array(
			'title' => 'Data Nilai Ujian Nasional',
			'content' => $this->load->view('admin/un', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_add()
	{
		$add_un = $this->M_Un->save();

		if ($add_un) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}

		redirect ('admin/Un');
	}

	public function do_edit()
	{
		$edit = $this->M_Un->update();
		
		if ($edit) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('admin/Un');	
	}

	public function delete($id_un)
	{
		$delete = $this->M_Un->delete($id_un);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('admin/Un');
	}

	public function export_excel()
	{
		$data = array( 'title' => 'Laporan Excel',
			'uns' => $this->M_Un->getAll());
		$this->load->view('admin/report/un_excel',$data);
	}
	
}
