<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Raport extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_NilaiAkhir');
		$this->load->model('M_Raport');
		if($this->session->userdata('state') != "admin")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$data['raports'] = $this->M_Raport->getAll();
		$data_to_template = array(
			'title' => 'Data Raport',
			'content' => $this->load->view('admin/raport', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);
		$this->load->view('layouts/template', $data_to_template);	
	}
	
	public function generate_raport(){
		$data_raports = $this->M_NilaiAkhir->getDataRaport();
		$cek = count($this->M_Raport->getAll());
		if ($cek != 0){
			$this->M_Raport->empty_table();
		}
		foreach ($data_raports as $data_raport) 
		{
			$this->siswa_id = $data_raport->siswa_id;
			$this->mapel_id = $data_raport->mapel_id;
			$this->nilai = $data_raport->total;
			$this->db->insert('tb_raport', $this);
		}
		$msg = "Generate raport berhasil";
		echo json_encode(array('msg' => $msg));
	}

	public function export_excel()
	{
		$data = array( 'title' => 'Laporan Excel',
			'raports' => $this->M_Raport->getAll());
		$this->load->view('admin/report/raport_excel',$data);
	}
}
