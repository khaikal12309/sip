<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//load model admin
		$this->load->model('M_Auth');
		// if (! $this->session->userdata()){
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
		// if($this->session->userdata('state') == "siswa")
		// {
		// 	redirect("siswa/SiswaDashboard");
		// } elseif ($this->session->userdata('state') == "guru") {
		// 	redirect("guru/GuruDashboard");
		// } elseif ($this->session->userdata('state') == "admin") {
		// 	redirect("admin/AdminDashboard");
		// } else{
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
	}
	public function index()
	{
		$data_to_template = array(
			'title' => 'Home',
			'content' => $this->load->view('home', null, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);
		$this->load->view('layouts_front/template', $data_to_template);
	}

	public function do_login()
	{
		// Tampung data
		$data['username'] = $this->input->post('username');
		$data['password'] = MD5($this->input->post('password'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		} else {
			// Cek login
			$checking = $this->M_Auth->check_login($data);			
			if($checking->num_rows() > 0){
				$data = $checking->row();

				
				// cek status
				if ($data->is_active == True){
					$session_data = array(
						'id_user'   => $data->id_user,
						'email'   => $data->email,
						'name_user' => $data->name_user,
						'username' => $data->username,
						'password' => $data->password,
						'state' => $data->state,
						'image' => $data->image,
						'is_active' => $data->is_active,
					);

					//set session userdata
					$this->session->set_userdata($session_data);
					if ($data->state == 'admin') {
						redirect('admin/admindashboard');
					}else if ($data->state == 'guru'){
						$this->session->set_flashdata('success', 'Selamat Datang '.$this->session->userdata('name_user').'');
						redirect('guru/GuruDashboard');
					}else{
						$this->session->set_flashdata('success', 'Selamat Datang '.$this->session->userdata('name_user').'');
						redirect('siswa/SiswaDashboard');
					}
				} else{
					$this->session->set_flashdata('error', 'Akun sudah tidak aktif');
					$referred_from = $this->session->userdata('referred_from'); 
					redirect($referred_from, 'refresh');
				}
			}else{
				$this->session->set_flashdata('error', 'Username atau Password salah');
				$referred_from = $this->session->userdata('referred_from'); 
				redirect($referred_from, 'refresh');
			}
		}
		
	}

	public function logout()
    {
		$this->session->sess_destroy();
		redirect('auth'); 
    }
}
