<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Mapel');
		$this->load->model('M_Kelas');
		if($this->session->userdata('id_user') == Null)
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
	
	public function index()
	{
		$id_user = $this->session->userdata('id_user');
		$data['user'] = $this->M_User->getById($id_user);

		
		$data_to_template = array(
			'title' => 'Profile',
			'url' => 'profile',
			'content' => $this->load->view('profile', $data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function profile()
	{
		$id_user = $this->session->userdata('id_user');
		$data['user'] = $this->M_User->getById($id_user);
		$data['mapels'] = $this->M_Mapel->getMapelByGuru($id_user);
		$data['kelas'] = $this->M_Kelas->getMyKelas();
		
		$data_to_template = array(
			'title' => 'Profile',
			'url' => 'profile',
			'content' => $this->load->view('profile_front', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);

		$this->load->view('layouts_front/template', $data_to_template);	
	}

	public function do_update(){
		$update = $this->M_User->update();
		if ($update){
			$this->session->set_flashdata('success', 'Data berhasil diubah');
			
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}
		redirect ('my');
	}

	public function do_update_front(){
		$update = $this->M_User->update();
		if ($update){
			$this->session->set_flashdata('success', 'Data berhasil diubah');
			
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}
		redirect ('My/profile');
	}

	public function change_password()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('old_password' , 'Old Password', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', 'Data gagal diubah');
		} 
		else 
		{
			$cek = $this->M_User->cek();
			if ($cek)
			{
				$change_password = $this->M_User->update_pass();
				if ($change_password) 
				{
					$this->session->set_flashdata('success', 'Password berhasil diubah');
				}
				else
				{
					$this->session->set_flashdata('error', 'Password gagal diubah');
				}	
			}
			else
			{
				$this->session->set_flashdata('error', 'Password gagal diubah');
			}
			
		}
		if ($this->input->post('redirect') == 'front'){
			redirect ('My/profile');
		}else{
			redirect ('My');
		}
	}

}
