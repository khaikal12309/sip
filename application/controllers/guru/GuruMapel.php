<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruMapel extends CI_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see https://codeigniter.com/user_guide/general/urls.html
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_Nilai');
		$this->load->model('M_Tipe_nilai');
		if($this->session->userdata('state') != "guru")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
	}

	public function index()
	{
		$guru_id = $this->session->userdata('id_user');
		$data['mapels'] = $this->M_Mapel->getMapelByGuru($guru_id);
		$data_to_template = array(
			'title' => 'Kelas',
			'content' => $this->load->view('guru/gurumapel', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);

	// print_r($data_to_template['title']);
	// die();

		$this->load->view('layouts_front/template', $data_to_template);	
	}

	
}
