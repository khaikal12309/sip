<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruNilai extends CI_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see https://codeigniter.com/user_guide/general/urls.html
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_Nilai');
		$this->load->model('M_Tipe_nilai');
		if($this->session->userdata('state') != "guru")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
	}

	public function index()
	{
		$data['nilais'] = $this->M_Nilai->getNilaiByGuru();
		// print_r($data['nilais'][3]->nama_guru);
		// die();
		$data['my_kelas'] = $this->M_Kelas->getMyKelas();
		$data_to_template = array(
			'title' => 'Kelas',
			'content' => $this->load->view('guru/gurunilai', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);

	// print_r($data_to_template['title']);
	// die();

		$this->load->view('layouts_front/template', $data_to_template);	
	}		

	public function delete($id_nilai)
	{
		$delete = $this->M_Nilai->delete($id_nilai);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('guru/GuruNilai');
	}

	public function cetak()
	{
		$data['nilais'] = $this->M_Nilai->getNilaiByGuru();
		$data = array( 'title' => 'PrintDataNilai');
		$this->load->view('admin/report/nilai_excel',$data);
	}
}
