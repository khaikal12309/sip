<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruKelas extends CI_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see https://codeigniter.com/user_guide/general/urls.html
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_User');
		$this->load->model('M_Kelas');
		$this->load->model('M_Mapel');
		$this->load->model('M_Nilai');
		$this->load->model('M_Tipe_nilai');
		$this->load->model('M_Un');
		$this->load->model('M_NilaiAkhir');
		$this->load->model('M_Raport');
		if($this->session->userdata('state') != "guru")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
	}

	public function index()
	{
		$data['kelass'] = $this->M_Kelas->getKelas();
		$data['my_kelas'] = $this->M_Kelas->getMyKelas();
		$data['mapel_uns'] = $this->M_Mapel->getMapelUN();
		$data['nilai_uns'] = $this->M_Un->getAll();
		$data_to_template = array(
			'title' => 'Kelas',
			'content' => $this->load->view('guru/gurukelas', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);

		$this->load->view('layouts_front/template', $data_to_template);	
	}

	public function lihat_siswa($kelas_id)
	{
		$data['siswas'] = $this->M_User->GetByKelas($kelas_id);
		$data['mapels'] = $this->M_Mapel->GetByGuru($kelas_id);
		$data['tipes'] = $this->M_Tipe_nilai->getAll();
		// print_r($data['mapels']);
		// die();
		$data_to_template = array(
			'title' => 'Siswa',
			'content' => $this->load->view('guru/lihat_siswa', $data, TRUE),
			'header' => $this->load->view('layouts_front/header', null, TRUE),
			'footer' => $this->load->view('layouts_front/footer', null, TRUE),
		);
		$this->session->set_flashdata('url', ''.base_url('index.php/guru/GuruKelas/lihat_siswa/'.$kelas_id.'').'');
		$this->load->view('layouts_front/template', $data_to_template);	
	}

	public function update_siswa()
	{
		$edit = $this->M_User->update();
		
		if ($edit) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('guru/GuruKelas');	
	}

	public function cetak_siswa()
	{
		$data = array( 'title' => 'Data Siswa',
			'users' => $this->M_Kelas->getMyKelas());
		$this->load->view('admin/report/user_excel',$data);
	}

	public function cetak_un()
	{
		$data = array( 'title' => 'Data UN',
			'uns' => $this->M_Un->GetByGuru());
		$this->load->view('admin/report/un_excel',$data);
	}

	public function cetak_na()
	{
		$data = array( 'title' => 'Data Nilai Akhir',
			'nilai_akhirs' => $this->M_NilaiAkhir->getMyNa());
		$this->load->view('admin/report/nilai_akhir',$data);
	}

	public function cetak_nr()
	{
		$data = array( 'title' => 'Data Nilai Raport',
			'raports' => $this->M_Raport->getMyRaport());
		$this->load->view('admin/report/raport_excel',$data);
	}


	public function check_add()
	{
		$code = 0;
		$msg= '';
		$id_tipe_nilai= $this->input->post('tipe_id');
		$tipe_nilai = $this->db->query('SELECT * from tb_tipe_nilai where id_tipe_nilai = '.$id_tipe_nilai.' limit 1')->result();

		if ($tipe_nilai[0]->is_multi_input == False){
			$cek = $this->M_Nilai->cek();
			if ($cek->num_rows()==0)
			{
				$code=1;
			}else{
				$code = 0;
				$msg = 'Data nilai pada tipe nilai tersebut telah tesedia';
			}
		}else{
			$code=1;
		}
		echo json_encode(array('msg' => $msg,'code' => $code));
	}

	public function add_nilai()
	{
		$add_nilai = $this->M_Nilai->save();
		if ($add_nilai) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}
		redirect ('guru/GuruKelas');
	}

	// UN
	public function show_un(){
		$code = 0;
		$data_table = $this->data_un();
		echo json_encode(array('data_table' => $data_table,'code' => 1));
		exit();
	}

	public function data_un(){
		$data_table2='';
		$data_table1='';
		$data_table3='';
		$data_un = $this->M_Un->GetByGuru();
		$no = 0;   
        foreach ($data_un as $un) {
        $no++;
         $data_table2 .='
            <tr>
                <td>'.$no.'</td>
                <td>'.$un->name_user.'</td>
                <td>'.$un->nama_mapel.'</td>
                <td>'.$un->nilai_un.'</td>
                <td>
                <a class="genric-btn warning arrow medium" href="#edit_un'.$un->id_un.'" data-toggle="modal">Edit
                </a>
                <a class="genric-btn danger arrow medium" href="#delete_un'.$un->id_un.'" data-toggle="modal">Delete
                </a>
                </td>
            </tr>
        ';
    	}
    	$data_table1 .='
    	<a class="genric-btn success arrow medium" href='.base_url('index.php/guru/GuruKelas/cetak_un').'>Cetak</a>
    	<a class="genric-btn info arrow medium" href="#tambah_un" data-toggle="modal"">Tambah Data</a>
    	<br><br>
    	<table id="table_un" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Nilai Ujian Nasional</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>';
        $data_table3 .="</tbody></table>";
        $data_table=$data_table1.''.$data_table2.''.$data_table3 ;
        
        return $data_table;
	}

	public function add_un()
	{
		$code=0;
		$add_un = $this->M_Un->save();

		if ($add_un) {
			$code=1;
		}else{
			$code=0;
		}
		echo json_encode(array('code' => $code));
	}

	public function delete_un()
	{
		$code=0;
		$id_un = $this->input->post('id_un');
		$add_un = $this->M_Un->delete($id_un);

		if ($add_un) {
			$code=1;
		}else{
			$code=0;
		}
		echo json_encode(array('code' => $code));
	}

	public function update_un()
	{
		$code=0;
		$update_un = $this->M_Un->update();

		if ($update_un) {
			$code=1;
		}else{
			$code=0;
		}
		echo json_encode(array('code' => $code));
	}

	// End UN

	// Siswa

	public function show_siswa(){
		$code = 0;
		$data_table = $this->data_siswa();
		echo json_encode(array('data_table' => $data_table,'code' => 1));
	}

	public function data_siswa(){
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_siswas = $this->M_Kelas->getMyKelas();
		$no = 0;   
        foreach ($data_siswas as $user) {
        $no++;
         $data_table2 .='
            <tr>
                <td>'.$user->ni.'</td>
                <td>'.$user->name_user.'</td>
                <td>'.$user->name_kelas.'</td>
                <td>'.$user->tpt_lahir.', '.$user->tgl_lahir.'</td>
                <td>'.$user->alamat.'</td>
                <td> <img src="'.base_url('images/user/'.$user->image.'').'" width="100%"></td>
                <td>
                <a class="genric-btn warning arrow medium" href="#edit_siswa'.$user->id_user.'" data-toggle="modal">Edit
                </a>
                </td>
            </tr>
        ';
    	}
    	$data_table1 .='
    	<a class="genric-btn success arrow medium" href='.base_url('index.php/guru/GuruKelas/cetak_siswa').'>Cetak</a>
    	<br><br>
    	<table id="table_kelas_saya" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Nomor Induk</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Tempat, Tanggal Lahir</th>
                <th>Alamat</th>
                <th style="width:10%">Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>';
        $data_table3 .="</tbody></table>";
        $data_table=$data_table1.''.$data_table2.''.$data_table3 ;
        return $data_table;
	}

	// Show Nilai Akhir

	public function show_na(){
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_nas = $this->M_NilaiAkhir->getMyNa();
		$no = 0;   
        foreach ($data_nas as $na) {
        $no++;
         $data_table2 .='
            <tr>
            	<td>'.$no.'</td>
                <td>'.$na->name_user.'</td>
                <td>'.$na->name_mapel.'</td>
                <td>'.$na->nama_tipe_nilai.'</td>
                <td>'.$na->nilai_akhir.'</td>
                <td>'.$na->last_update.'</td>
            </tr>
        ';
    	}
    	$data_table1 .='
    	<a class="genric-btn success arrow medium" href='.base_url('index.php/guru/GuruKelas/cetak_na').'>Cetak</a>
    	<br><br>
    	<table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mata Pelajaran</th>
                <th>Tipe Penilaian</th>
                <th>Nilai Akhir</th>
                <th>Last Update</th>
            </tr>
        </thead>
        <tbody>';
        $data_table3 .="</tbody></table>";
        $data_table=$data_table1.''.$data_table2.''.$data_table3 ;
        if ($data_table){
        	$code=1;
        }
        echo json_encode(array('data_table' => $data_table,'code' => $code));
	}

	// Show Nilai Akhir

	public function show_nr(){
		$code = 0;
		$data_table1 ='';
		$data_table2 ='';
		$data_table3 ='';

		$data_nrs = $this->M_Raport->getMyRaport();
		$no = 0;   
        foreach ($data_nrs as $nr) {
        $no++;
         $data_table2 .='
            <tr>
            	<td>'.$no.'</td>
                <td>'.$nr->name_user.'</td>
                <td>'.$nr->name_mapel.'</td>
                <td>'.$nr->nilai.'</td>
                <td>'.$nr->last_update.'</td>
            </tr>
        ';
    	}
    	$data_table1 .='
    	<a class="genric-btn success arrow medium" href='.base_url('index.php/guru/GuruKelas/cetak_nr').'>Cetak</a>
    	<br><br>
    	<table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mata Pelajaran</th>
                <th>Nilai Akhir</th>
                <th>Last Update</th>
            </tr>
        </thead>
        <tbody>';
        $data_table3 .="</tbody></table>";
        $data_table=$data_table1.''.$data_table2.''.$data_table3 ;
        if ($data_table){
        	$code=1;
        }
        echo json_encode(array('data_table' => $data_table,'code' => $code));
	}
}
