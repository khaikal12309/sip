<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url();?>images/user/<?php echo $this->session->userdata('image');?>" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo $this->session->userdata('name_user');?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li class="<?php if (current_url() == "http://localhost/ci-sip/index.php/admin/admindashboard") { ?>active<?php } else{ echo'';}?> ?>">

				<a href="<?php echo base_url();?>index.php/admin/admindashboard">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-pie-chart"></i>
					<span>Master Data</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url('index.php/admin/User');?>"><i class="fa fa-circle-o"></i> Data Siswa</a></li>
					<li><a href="<?php echo base_url('index.php/admin/Guru');?>"><i class="fa fa-circle-o"></i> Data Guru</a></li>
					<li><a href="<?php echo base_url('index.php/admin/Kelas');?>"><i class="fa fa-circle-o"></i> Data Kelas</a></li>
					<li><a href="<?php echo base_url('index.php/admin/Mapel');?>"><i class="fa fa-circle-o"></i> Data Mata Pelajaran</a></li>
					<li><a href="<?php echo base_url('index.php/admin/Tipe_nilai');?>"><i class="fa fa-circle-o"></i> Data Tipe Penilaian</a></li>
				</ul>
				
			</li>
			<li class="<?php if (current_url() == "http://localhost/ci-sip/index.php/admin/Nilai") { ?>active<?php }?>">
				<a href="<?php echo base_url();?>index.php/admin/Nilai">
					<i class="fa fa-pencil"></i> <span>Penilaian</span>
				</a>
			</li>
			<li class="<?php if (current_url() == "http://localhost/ci-sip/index.php/admin/NilaiAkhir") { ?>active<?php }?>">
				<a href="<?php echo base_url();?>index.php/admin/NilaiAkhir">
					<i class="fa fa-check-circle-o"></i> <span>Nilai Akhir</span>
				</a>
			</li>
			<li class="<?php if (current_url() == "http://localhost/ci-sip/index.php/admin/Un") { ?>active<?php }?>">
				<a href="<?php echo base_url();?>index.php/admin/Un">
					<i class="fa fa-graduation-cap"></i> <span>Nilai Ujian Nasional</span>
				</a>
			</li>
			<li class="<?php if (current_url() == "http://localhost/ci-sip/index.php/admin/Raport") { ?>active<?php }?>">
				<a href="<?php echo base_url();?>index.php/admin/Raport">
					<i class="fa fa-book"></i> <span>Raport</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>