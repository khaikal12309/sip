<header class="main-header">
	<!-- Logo -->
	<a href="dashboard.html" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>S</b>IP</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Sip</b>LTE</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown full right">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <?php echo $this->session->userdata("name_user")?> <span class="caret"></span>
	              </a>
	              <ul class="dropdown-menu" role="menu">
	                <li><a href="<?php echo base_url();?>index.php/my" ><i class="fa fa-user"></i>Profile</a></li>
	                <li class="divider"></li>
	                <li><a href="<?php echo base_url();?>index.php/auth/logout"><i class="fa fa-sign-out"></i>Logout</a></li>
	              </ul>
	            </li>
			</ul>
		</div>


	</nav>
</header>