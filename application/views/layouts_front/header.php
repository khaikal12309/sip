<header>
	<div class="header-area ">
		<div class="main-header-area" style="padding-left: 10px;padding-right: 10px">
			<div class="container-fluid p-0">
				<div class="row align-items-center no-gutters">
					<div class="col-xl-2 col-lg-2" >
						<div class="logo-img">
							<a href="index.html">
								<img src="<?php echo base_url()?>assets2/img/logo.png" alt="" width="50%">
							</a>
						</div>
					</div>
					<div class="col-xl-7 col-lg-7">
						<div class="main-menu  d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a <?php if (current_url() == base_url('index.php') or current_url() == base_url('index.php/siswa/SiswaDashboard') or current_url() == base_url('index.php/guru/GuruDashboard')){echo "class='active'";}?> href="<?php echo base_url('index.php');?>">Home</a></li>
									<?php if($this->session->userdata("id_user")){?>
									<?php if ($this->session->userdata("state") == 'guru') {?>
										<li>
											<a <?php if (current_url() == base_url('index.php/guru/GuruKelas') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?>  href="<?php echo base_url('index.php/guru/GuruKelas')?>">Kelas</a>
										</li>
										<li><a <?php if (current_url() == base_url('index.php/guru/GuruMapel') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?> href="<?php echo base_url('index.php/guru/GuruMapel')?>">Mata Pelajaran</a></li>
										<li><a <?php if (current_url() == base_url('index.php/guru/GuruNilai') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?> href="<?php echo base_url('index.php/guru/GuruNilai')?>">Penilaian</a></li>
									<?php } ?>
										<?php if ($this->session->userdata("state") == 'siswa') {?>
										<li><a <?php if (current_url() == base_url('index.php/siswa/SiswaNilai') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?> href="<?php echo base_url('index.php/siswa/SiswaNilai')?>">Nilai Saya</a></li>
									<?php } ?>
										<li><a <?php if (current_url() == base_url('index.php/My/profile') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?> href="<?php echo base_url('index.php/My/profile')?>">Profil</a></li>
									<?php }?>
									
									<li><a <?php if (current_url() == base_url('index.php/Welcome/about') or current_url() == $this->session->flashdata("url")){echo "class='active'";}?>  href="<?php echo base_url('index.php/Welcome/about')?>">About</a></li>


									
								</ul>
							</nav>
						</div>
					</div>
					<div class="col-xl-3 col-lg-3 d-none d-lg-block">
						<div class="log_chat_area d-flex align-items-center">
							<?php if (! $this->session->userdata("id_user")) {?> 
								<a href="#test-form" class="login popup-with-form">
									<i class="flaticon-user"></i>
									<span>log in</span>
								</a>
							<?php }else{ ?>
								<a href="<?php echo base_url();?>index.php/Auth/logout" class="login">
									<i class="flaticon-user"></i>
									<span>log out</span>
								</a>
							<?php }?>
						</div>
					</div>
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<!-- form itself end-->
<div id="test-form" class="white-popup-block mfp-hide">
	<div class="popup_box ">
		<div class="popup_inner">
			<div class="logo text-center">
				<a href="#">
					<img src="<?php echo base_url();?>assets2/img/logo.png" alt="" width="50%">
				</a>
			</div>
			<h3>Sign in</h3>
			<form method="post" action="<?php echo base_url()?>index.php/Auth/do_login" >
				<div class="row">
					<div class="col-xl-12 col-md-12">
						<input type="text" name="username" placeholder="Enter Username">
					</div>
					<div class="col-xl-12 col-md-12">
						<input type="password" name="password" placeholder="Password">
					</div>
					<div class="col-xl-12">
						<button type="submit" class="boxed_btn_orange">Sign in</button>
					</div>
					<div class="col-xl-12">
						<p style="color: white">Lupa password? Hubungi <a href="https://api.whatsapp.com/send?phone=0895342960174">Admin</p></a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- form itself end -->