<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SIP | <?php echo (isset($title)) ? $title : '';?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets2/img/logo.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    

    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/gijgo.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/slicknav.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>sweetalert/dist/sweetalert.css" />
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets2/css/responsive.css"> -->
</head>

<body>
    <div class="preloader" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: #fff;
    display:none;">
        <div class="loading" style="position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        font: 14px arial;">
        <img src="<?php echo base_url()?>images/pre.gif" width="500px">
        <center><p>Harap Tunggu</p></center>
        </div>
    </div>
    <div style="display: none;" id="div1">
        <?php echo $header;?>
        <?php echo $content;?>
        <?php echo $footer;?>
    </div>

    <!-- JS here -->
    <script src="<?php echo base_url()?>assets2/js/vendor/modernizr-3.5.0.min.js"></script>
    
    <script src="<?php echo base_url()?>assets2/js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/ajax-form.js"></script>
    <script src="<?php echo base_url()?>assets2/js/waypoints.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/scrollIt.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/wow.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/nice-select.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.slicknav.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/plugins.js"></script>
    <script src="<?php echo base_url()?>assets2/js/gijgo.min.js"></script>

    <!--contact js-->
    <script src="<?php echo base_url()?>assets2/js/contact.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.form.js"></script>
    <script src="<?php echo base_url()?>assets2/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>assets2/js/mail-script.js"></script>

    <script src="<?php echo base_url()?>assets2/js/main.js"></script>
    <script src="<?php echo base_url();?>sweetalert/dist/sweetalert.js"></script>
    <script>
    $(document).ready(function(){
        $(".preloader").fadeIn();
        $(".preloader").fadeOut('slow', function(){
            document.getElementById('div1').style.display = 'block';
            <?php if($this->session->flashdata('success')):?>
                swal("Good job!", "<?=$this->session->flashdata('success') ?>", "success");
            <?php endif ?>
            <?php if($this->session->flashdata('error')):?>
                swal("Oopps!", "<?=$this->session->flashdata('error') ?>", "error");
            <?php endif ?>
        });
    })
    </script>
    
</body>

</html>