<div class="courses_details_banner" style="padding-top: 150px;padding-bottom: 100px;">
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<div class="course_text">
					<h3><?php echo $this->session->userdata('name_user');?></h3>
					<div class="prise">
						<span class="active"><?php echo $this->session->userdata('state');?></span>
					</div>
				</div>
			</div>
			<div class="col-xl-6" style="padding-left: 250px">
				<img src="<?php echo base_url('images/user/'.$user->image.'');?>" width="200px"alt="">
			</div>
		</div>
	</div>
</div>
<!-- bradcam_area_end -->

<div class="courses_details_info" style="padding-top: 100px">
	<div class="container">
		<div class="row">
			<div class="col-xl-7 col-lg-7">
				<div class="outline_courses_info">
					<a class="genric-btn info arrow lg" href="#change_password"data-toggle="modal">Change Password</a>
					<br/><hr/>
					<?php if ($this->session->userdata('state') == 'guru'){?>
						<div id="accordion">
							<div class="card">
								<div class="card-header" id="headingTwo">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											<i class="flaticon-question"></i> Mata Pelajaran?
										</button>
									</h5>
								</div>
								<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
									<div class="card-body">
										<?php foreach($mapels as $mapel):?>
											<p><?php echo $mapel->nama_mapel?> - <?php echo $mapel->tingkat?> - <?php echo $mapel->jurusan?></p>
										<?php endforeach?>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="headingOne">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
											<i class="flaticon-question"></i>Wali Kelas?</span>
										</button>
									</h5>
								</div>
								<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
									<div class="card-body">
										<p>Wali kelas kelas <?php echo $kelas[0]->name_kelas?></p>
									</div>
								</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5">
				<div class="outline_courses_info">
				<form action="<?php echo base_url('index.php/My/do_update_front')?>" enctype="multipart/form-data" method="POST">
					<div class="feedback_info">
						<h3>Profile</h3>
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-at" aria-hidden="true"></i></div>
							<input type="text" name="email" placeholder="Email" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Email'" required class="single-input" value="<?php echo $user->email;?>" <?php if ($this->session->userdata('state') == 'siswa'){ echo "readonly";}?>>
						</div>
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
							<input type="text" name="phone" placeholder="Telephone" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Telephone'" required class="single-input" value="<?php echo $user->phone;?>" <?php if ($this->session->userdata('state') == 'siswa'){ echo "readonly";}?>>
						</div>
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
							<input type="date" name="tgl_lahir" placeholder="Tanggal Lahir" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Tanggal Lahir'" value="<?php echo $user->tgl_lahir;?>" required class="single-input" <?php if ($this->session->userdata('state') == 'siswa'){ echo "readonly";}?>>
						</div>
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-leaf" aria-hidden="true"></i></div>
							<input type="text" name="tpt_lahir" placeholder="Tempat Lahir" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Tempat Lahir'" value="<?php echo $user->tpt_lahir;?>" required class="single-input" <?php if ($this->session->userdata('state') == 'siswa'){ echo "readonly";}?>>
						</div>
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
							<input type="text" name="alamat" placeholder="Address" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Address'" value="<?php echo $user->alamat;?>" required class="single-input" <?php if ($this->session->userdata('state') == 'siswa'){ echo "readonly";}?>>
						</div>
						<?php if ($this->session->userdata('state') == 'guru'){?>
						<div class="input-group-icon mt-10">
							<input type="hidden" name="old_image" value="<?php echo $user->image;?>">
							<div class="icon"><i class="fa fa-image" aria-hidden="true"></i></div>
							<input type="file" name="image" class="single-input">
						</div>
						<br/>

						<input type="hidden" name="name_user" value="<?php echo $user->name_user;?>">
						<input type="hidden" name="ni" value="<?php echo $user->ni;?>">
						<input type="hidden" name="id_user" value="<?php echo $user->id_user;?>">
						
							<button type="submit" class="boxed_btn" style="width: 100%">Update</button><br/><br/>
						<?php } ?>

					</div>
				</form>
			</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(function () {
		$('#example1').DataTable()
		$('#example2').DataTable()
	})
</script>
</script>

<div class="modal modal-warning fade" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/my/change_password">
                <input type="hidden" name="id_user" value="<?php echo $user->id_user?>">
                <input type="hidden" name="redirect" value="front">
                <div class="modal-header">
                	<h4 class="modal-title">Change Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        
                </div>
                <div class="modal-body">

                	<div class="input-group-icon mt-10">
						<div class="icon"><i class="fa fa-database" aria-hidden="true"></i></div>
						<input type="password" name="old_password" placeholder="Password" onfocus="this.placeholder = ''"
						onblur="this.placeholder = 'Password'" required class="single-input">
					</div>
					<div class="input-group-icon mt-10">
						<div class="icon"><i class="fa fa-key" aria-hidden="true"></i></div>
						<input type="password" name="password" placeholder="Password Baru" onfocus="this.placeholder = ''"
						onblur="this.placeholder = 'Password Baru'" required class="single-input">
					</div>
					<div class="input-group-icon mt-10">
						<div class="icon"><i class="fa fa-key" aria-hidden="true"></i></div>
						<input type="password" name="passconf" placeholder="Konfirmasi Password" onfocus="this.placeholder = ''"
						onblur="this.placeholder = 'Konfirmasi Password'" required class="single-input">
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
                    <button type="submit" class="genric-btn success arrow medium">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>

