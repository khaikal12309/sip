<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <form method="POST" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/my/do_update">
                <div class="col-md-4">

                    <!-- Profile Image -->
                    <div class="box">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>images/user/<?php echo $user ->image?>" alt="User profile picture">
                            <h3 class="profile-username text-center"><?php echo $user->username?></h3>

                            <p class="text-muted text-center"><?php echo $user->state?></p>
                            <input type="file" name="image" class="form-control">
                            <input type="hidden" name="old_image" value="<?php echo $user->image?>">
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </div>
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <ul class="list-group list-group-unbordered">
                                <div class="form-group">
                                    <label>Name</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" name="name_user" class="form-control" value="<?php echo $user -> name_user ;?>">
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group">
                                    <label>Phone</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="text" name="phone" class="form-control" value="<?php echo $user -> phone ;?>">
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group">
                                    <label>Email</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa  fa-envelope"></i>
                                        </div>
                                        <input type="email" class="form-control" name="email" value="<?php echo $user -> email ;?>">
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group">
                                    <label>Alamat</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-location-arrow"></i>
                                        </div>
                                        <!-- <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""> -->
                                        <textarea class="form-control" name="alamat"><?php echo $user -> alamat ;?></textarea>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <input type="hidden" name="id_user" value="<?php echo $user->id_user?>">
                                <button type="submit" class="btn btn-success">Update</button>
                                <button type="button" class="btn btn-warning" href="#change_password"data-toggle="modal">Change Password</button>                            

                            </ul>
                        </div>
                    </div>
                    
                </div>
            </form>
            <!-- /.col -->
           
        </div>
                <!-- /.row -->

    </section>
            <!-- /.content -->
</div>

<div class="modal modal-warning fade" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/my/change_password">
                <input type="hidden" name="id_user" value="<?php echo $user->id_user?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Password</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-database"></i>
                            </div>
                            <input type="password" name="old_password" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>New Password</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Confirm Password</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <input type="password" name="passconf" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>