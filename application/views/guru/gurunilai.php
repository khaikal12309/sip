<div class="bradcam_area breadcam_bg overlay2">
	<h3>Data Penilaian <?php echo $this->session->userdata("name_user");?></h3>
</div>
<!-- bradcam_area_end -->

<!-- popular_courses_start -->
<div class="popular_courses">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="box-header">
					<a class="btn btn-success" href="<?php echo base_url('index.php/guru/GuruNilai/cetak_excel')?>"><i class="fa fa-book"></i> Cetak</a>
                    <br/><br/>
				</div>
				<div class="box">
					<div class="box-body">
					<table id="example1" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Siswa</th>
                                    <th>Tipe Nilai</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Nilai</th>
                                    <th>Action</th>
                                    <th>Diinput Oleh</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($nilais as $nilai):
                                    if ($this->session->flashdata('id_nilai'))
                                    {
                                        if ($nilai->id_nilai == $this->session->flashdata('id_nilai'))
                                    {
                                ?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $nilai->name_user;?></td>
                                        <td><?php echo $nilai->nama_tipe_nilai;?></td>
                                        <td><?php echo $nilai->nama_mapel;?></td>
                                        <td><?php echo $nilai->nilai;?></td>
                                        <td>
                                            <a class="genric-btn warning arrow medium" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal">Edit</a>
                                            <a class="genric-btn danger arrow medium" href="<?php echo base_url()."index.php/guru/GuruNilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                        </td>
                                        <td><?php echo $nilai->input_oleh;?></td>
                                    </tr>
                                <?php 
                                    } 
                                    } else{
                                ?>
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $nilai->name_user;?></td>
                                        <td><?php echo $nilai->nama_tipe_nilai;?></td>
                                        <td><?php echo $nilai->nama_mapel;?></td>
                                        <td><?php echo $nilai->nilai;?></td>
                                        <td>
                                            <a class="genric-btn warning arrow medium" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal">Edit</a>
                                            <a class="genric-btn danger arrow medium" href="<?php echo base_url()."index.php/guru/GuruNilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');">Hapus</a>
                                        </td>
                                        <td><?php echo $nilai->input_oleh;?></td>
                                    </tr>
                            <?php } ?>
                                <?php endforeach;?>
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($nilais as $n):?>
<!-- Modal Edit -->
    <div class="modal modal-warning fade" id="edit<?php echo $n->id_nilai?>">
        <div class="modal-dialog">
            <div class="modal-content" style="width:120%">
                <form method="POST" action="<?php echo base_url();?>index.php/guru/GuruNilai/edit_nilai" enctype="multipart/form-data">
                    <input type="hidden" name="id_nilai" id="id_nilai" value="<?php echo $n->id_nilai?>">
                    <div class="modal-header">
                        <h3>Edit Nilai <?php echo $n->name_user;?></h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
                    </div>
                    <div class="modal-body">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                            <input type="text" name="nilai" placeholder="Nilai" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = 'Nilai'" value="<?php echo $n->nilai;?>"required class="single-input">
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
                        <button type="submit" class="genric-btn success arrow medium">Save changes</button>
                    </div>
                </form>
            </div>
                <!-- /.modal-content -->
        </div>
            <!-- /.modal-dialog -->
    </div>
<?php endforeach;?>

<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
  })
</script>
</script>

