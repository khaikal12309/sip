<div class="bradcam_area breadcam_bg overlay2">
	<h3>Data Mata Pelajaran <?php echo $this->session->userdata("name_user");?></h3>
</div>
<!-- bradcam_area_end -->

<!-- popular_courses_start -->
<div class="popular_courses">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="box-header">
					
				</div>
				<div class="box">
					<div class="box-body">
					<table id="example1" class="table table-striped">
						<thead>
                                <tr>
                                    <th>No</th>
                                    <th>KD Mapel</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Tingkat</th>
                                    <th>Jurusan</th>
                                    <th>Mapel UN?</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($mapels as $mapel):?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $mapel->kd_mapel;?></td>
                                        <td><?php echo $mapel->nama_mapel;?></td>
                                        <td><?php echo $mapel->tingkat;?></td>
                                        <td><?php echo $mapel->jurusan;?></td>
                                        <td>
                                            <?php if ($mapel->is_un){
                                                echo "Ya";
                                            }else{
                                                echo "Tidak";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
  })
</script>
</script>

