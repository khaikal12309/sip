<div class="bradcam_area breadcam_bg overlay2">
	<h3>Data Siswa Kelas <?php echo $siswas[0]->name_kelas;?></h3>
</div>
<!-- bradcam_area_end -->

<!-- popular_courses_start -->
<div class="popular_courses">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="box-header">
					
				</div>
				<div class="box">
					<div class="box-body">
					<table id="example1" class="table table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nomor Induk</th>
								<th>Nama Siswa</th>
								<th>Phone</th>
								<th>Action </th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach ($siswas as $m):?> 
								<tr>
									<td><?php echo $no;
									$no++;?></td>
									<td><?php echo $m->ni;?></td>
									<td><?php echo $m->name_user;?></td>
									<td><?php echo $m->phone;?></td>
									<td>
										<a class="genric-btn info circle arrow medium" href="#add_nilai<?php echo $m->id_user?>" data-toggle="modal"> Add Nilai</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php foreach ($siswas as $s):?>
<!-- Modal Edit -->
	<div class="modal modal-warning fade" id="add_nilai<?php echo $s->id_user?>">
	    <div class="modal-dialog">
	        <div class="modal-content" style="width:120%">
	            <form method="POST" action="<?php echo base_url();?>index.php/guru/GuruKelas/add_nilai" enctype="multipart/form-data">
	                <input type="hidden" name="siswa_id" id="siswa_id" value="<?php echo $s->id_user?>">
	                <input type="hidden" name="input_id" value="<?php echo $this->session->userdata('id_user');?>">
	                <div class="modal-header">
	                	<h3>Tambah Nilai <?php echo $s->name_user;?></h3>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span></button>
	                        <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
	                </div>
	                <div class="modal-body">
						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-book" aria-hidden="true"></i></div>
							<div class="form-select" id="default-select">
								<select name="mapel_id" id="mapel_id">
										<option value="1" selected="#">Mata Pelajaran</option>
										<?php foreach($mapels as $mapel):?>
											<option value="<?php echo $mapel->id_mapel;?>"><?php echo $mapel->nama_mapel;?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="input-group-icon mt-10">
							<div class="icon"><i class="fa fa-leaf" aria-hidden="true"></i></div>
							<div class="form-select" id="default-select">
								<select name="tipe_id" id="tipe_id">
									<option value="#" selected="#">Tipe Nilai</option>
									<?php foreach($tipes as $tipe):?>
										<option value="<?php echo $tipe->id_tipe_nilai;?>"><?php echo $tipe->nama_tipe_nilai;?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="input-group-icon mt-10" id="input_nilai" style="display: none">
							<div class="icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>
							<input type="text" name="nilai" placeholder="Nilai" onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Nilai'" required class="single-input">
						</div>
						<div class="alert alert-danger" id="alert" style="display: none">
							
						</div>
					</div>
					
	                <div class="modal-footer">
	                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
	                    <button type="submit" class="genric-btn success arrow medium">Save changes</button>
	                </div>
	            </form>
	        </div>
	            <!-- /.modal-content -->
	    </div>
	        <!-- /.modal-dialog -->
	</div>
<?php endforeach;?>
<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
  })

  $(document).ready(function () {

        $("#tipe_id").on('change', function () {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/check_add');?>',
                data: 'siswa_id='+$("#siswa_id").val()+'&mapel_id='+$("#mapel_id").val()+'&tipe_id='+$("#tipe_id").val(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                    	document.getElementById('input_nilai').style.display = 'block';
                    	document.getElementById('alert').style.display = 'none';
                    }else{
                    	$("#alert").html(json.msg)
                    	document.getElementById('alert').style.display = 'block';
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

    });



</script>
</script>

