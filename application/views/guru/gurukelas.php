<div class="bradcam_area breadcam_bg overlay2">
	<h3>Data Kelas</h3>
</div>
<!-- bradcam_area_end -->

<!-- popular_courses_start -->
<div class="popular_courses">
	<div class="container">
		<!-- <div class="row">
			<div class="col-xl-12">
				<div class="section_title text-center mb-100">
					<h3>Popular Courses</h3>
					<p>Your domain control panel is designed for ease-of-use and <br> allows for all aspects of your
					domains.</p>
				</div>
			</div>
		</div> -->
		<div class="row">
			<div class="col-12">
				<div class="course_nav">
					<nav>
						<ul class="nav" id="myTab" role="tablist" style="margin-bottom: 10px">
							<li class="nav-item">
								<a class="nav-link active" id="kelas-tab" data-toggle="tab" href="#kelas" role="tab"
								aria-controls="kelas" aria-selected="true">Kelas</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="kelas_saya-tab" data-toggle="tab" href="#kelas_saya" role="tab"
								aria-controls="kelas_saya" aria-selected="false">Kelas Saya</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="all_courses">
		<div class="container">
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="kelas" role="tabpanel" aria-labelledby="kelas-tab">
					<div class="row">
						<div class="col-xl-12">
							<div class="box-header">
								
							</div>
							<div class="box">
								<div class="box-body">
								<table id="table_kelas" class="table table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th>Kelas</th>
											<th>Nama Kelas</th>
											<th>Wali Kelas</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$no = 1;
										foreach ($kelass as $kelas):?> 
											<tr>
												<td><?php echo $no;
												$no++;?></td>
												<td><?php echo $kelas->tingkat;?> <?php echo $kelas->jurusan;?></td>
												<td><?php echo $kelas->name_kelas;?></td>
												<td><?php echo $kelas->name_user;?></td>
												<td>
													<a class="genric-btn info circle arrow medium" href="<?php echo base_url('index.php/guru/GuruKelas/lihat_siswa/'.$kelas->id_kelas.'')?>"> Lihat Siswa</a>
												</td>
											</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="kelas_saya" role="tabpanel" aria-labelledby="kelas_saya-tab">
					<div class="row">
						<div class="col-xl-12">
							<div class="box-header">
								
							</div>
							<div class="box">
								<div class="box-header">
									<center><h2>Data Siswa Kelas <?php echo $my_kelas[0]->name_kelas;?></h2></center>
									<hr/>
                                    <a class="genric-btn primary-border medium" href="#" id="btn-siswa">Data Siswa</a>
									<a class="genric-btn success-border medium" href="#" id="btn-un">Nilai UN</a>
									<a class="genric-btn info-border medium" id="btn-na" href="#">Nilai Akhir</a>
									<a class="genric-btn warning-border medium" id="btn-nr" href="#">Nilai Raport</a>
									<hr/>
								</div>
								<div class="box-body" id="data-table">
									<a class="genric-btn success arrow medium" href="<?php echo base_url('index.php/guru/GuruKelas/cetak_siswa')?>">Cetak</a><br><br>
									<table id="table_kelas_saya" class="table table-striped">
			                            <thead>
			                                <tr>
			                                    <th>Nomor Induk</th>
			                                    <th>Nama Siswa</th>
			                                    <th>Kelas</th>
			                                    <th>Tempat, Tanggal Lahir</th>
			                                    <th>Alamat</th>
			                                    <th style="width:10%">Image</th>
			                                    <th>Action</th>
			                                </tr>
			                            </thead>
			                            <tbody>
			                                <?php foreach ($my_kelas as $user):?> 
			                                    <tr>
			                                        <td><?php echo $user->ni;?></td>
			                                        <td><?php echo $user->name_user;?></td>
			                                        <td><?php echo $user->name_kelas;?></td>
			                                        <td><?php echo $user->tpt_lahir;?>, <?php echo $user->tgl_lahir;?></td>
			                                        <td><?php echo $user->alamat;?></td>
			                                        <td><img src="<?php echo base_url('images/user/'.$user->image.'')?>" width="100%"></td>
			                                        <td>
			                                            <a class="genric-btn warning arrow medium" href="#edit<?php echo $user->id_user?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
			                                        </td>
			                                    </tr>
			                                <?php endforeach;?>
			                            </tbody>
			                        </table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script>
  $(document).ready(function () {
    $('#table_kelas').DataTable({"lengthChange": false});
    $('#table_kelas_saya').DataTable({"lengthChange": false});
        function tampil_un(){
             $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_un');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
                        $("#btn-un").removeClass('genric-btn success-border medium').addClass('genric-btn success medium');
                        $("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
                        $("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
                    }else{
                        alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });
        }

        function tampil_siswa(){
             $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_siswa');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary-border medium').addClass('genric-btn primary medium');
                        $("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
                        $("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
                        $("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
                    }else{
                        alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });
        }

        $("#btn-siswa").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_siswa');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary-border medium').addClass('genric-btn primary medium');
                        $("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
                        $("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
                        $("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
                    }else{
                        alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        })

        $("#btn-na").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_na');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
                        $("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
                        $("#btn-na").removeClass('genric-btn info-border medium').addClass('genric-btn info medium');
                        $("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
                    }else{
                        alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

        $("#btn-nr").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_nr');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
                        $("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
                        $("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
                        $("#btn-nr").removeClass('genric-btn warning-border medium').addClass('genric-btn warning medium');
                    }else{
                        alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

  		// document.getElementById('data_table').style.display = 'block';
        $("#btn-un").on('click', function tampil_un() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/show_un');?>',
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                    	$("#data-table").html(json.data_table);
                        $("#btn-siswa").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
                        $("#btn-un").removeClass('genric-btn success-border medium').addClass('genric-btn success medium');
                        $("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
                        $("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
                    }else{
                    	alert('error');
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

        $("#btn-save-un").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/add_un');?>',
                data: $("#form-tambah-un").serialize(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                    	$('#tambah_un').modal('hide');
                    	swal("Good job!", "<?=$this->session->flashdata('Data berhasil disimpan') ?>", "success");
                    	tampil_un();
                    }else{
                    	swal("Oopps!", "<?=$this->session->flashdata('Nilai sudah tesedia') ?>", "error");
                    	tampil_un();
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

        $(".delete_un").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/delete_un');?>',
                data:$("#form-delete-un").serialize(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $('.modal_delete_un').modal('hide');
                    	swal("Good job!", "<?=$this->session->flashdata('Data berhasil dihapus') ?>", "success");
                    	tampil_un();
                    }else{
                    	swal("Oopps!", "<?=$this->session->flashdata('Data gagal dihapus') ?>", "error");
                    	tampil_un();
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });

        $(".edit_un").on('click', function() {
            $.ajax({
                url: '<?php echo base_url('index.php/guru/GuruKelas/update_un');?>',
                data:$("#form-edit-un").serialize(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    if (json.code == 1){
                        $('.modal_edit_un').modal('hide');
                        swal("Good job!", "<?=$this->session->flashdata('Data berhasil diubah') ?>", "success");
                        tampil_un();
                    }else{
                        swal("Oopps!", "<?=$this->session->flashdata('Data gagal diubah') ?>", "error");
                        tampil_un();
                    }
                },
                error: function () {
                    return false;
                }
            });

            return false;
        });


    });
</script>

<div class="modal modal-warning fade" id="tambah_un">
    <div class="modal-dialog">
        <div class="modal-content" style="width:120%">
            <form method="POST" id="form-tambah-un" enctype="multipart/form-data">
                <div class="modal-header">
                    <h3>Tambah Nilai UN</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
                </div>
                <div class="modal-body">
                	<div class="input-group-icon mt-10">
                        <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                        <div class="form-select" id="default-select">
	                       <select name="siswa_id">
	                       	<option selected value="#">-- Pilih Siswa --</option>
	                       	<?php foreach($my_kelas as $m):?>
	                       		<option value="<?php echo $m->id_user?>"><?php echo $m->name_user?></option>
	                       	<?php endforeach;?>
	                       </select>
	                   </div>
                    </div>
                    <div class="input-group-icon mt-10">
                        <div class="icon"><i class="fa fa-book" aria-hidden="true"></i></div>
                        <div class="form-select" id="default-select">
	                       <select name="mapel_id">
	                       	<option selected value="#">-- Pilih Mata Pelajaran --</option>
	                       	<?php foreach($mapel_uns as $un):?>
	                       		<option value="<?php echo $un->id_mapel?>"><?php echo $un->nama_mapel?></option>
	                       	<?php endforeach;?>
	                       </select>
	                   </div>
                    </div>
                    <div class="input-group-icon mt-10">
                        <div class="icon"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                        <input type="text" name="nilai_un" placeholder="Nilai" onfocus="this.placeholder = ''"
                            onblur="this.placeholder = 'Nilai'"required class="single-input">
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-save-un" class="genric-btn success arrow medium">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>

<?php foreach($my_kelas as $user):?>
<!-- Modal Edit -->
<div class="modal modal-warning fade" id="edit<?php echo $user->id_user?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/guru/GuruKelas/update_siswa" enctype="multipart/form-data">
                <input type="hidden" name="id_user" value="<?php echo $user->id_user?>">
                <div class="modal-header">
                	<h1>Edit Data</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
                </div>
                <div class="modal-body">
                    <div class="input-group-icon mt-10">
                        <label>Username</label>

                        <div class="input-group">
                            <input type="text" name="username" class="form-control" value="<?php echo $user->username?>" readonly>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Nomor Induk</label>

                        <div class="input-group">
                            <input type="text" name="ni" class="form-control" value="<?php echo $user->ni?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Email</label>
                        <div class="input-group">
                            <input type="email" name="email" class="form-control" value="<?php echo $user->email?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Nama Siswa</label>

                        <div class="input-group">
                            <input type="text" name="name_user" class="form-control" value="<?php echo $user->name_user?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Phone</label>

                        <div class="input-group">
                            <input type="text" name="phone" class="form-control" value="<?php echo $user->phone?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Kelas</label>

                        <div class="input-group">
                            <select class="form-control" name="kelas_id">
                                <?php foreach($kelass as $k):?>
                                    <option value="<?php echo $k->id_kelas?>" <?php if ($k->id_kelas == $user->kelas_id){echo "selected";}?>><?php echo $k->name_kelas?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Tempat Lahir</label>

                        <div class="input-group">
                            <input type="text" name="tpt_lahir" class="form-control" value="<?php echo $user->tpt_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Tanggal Lahir</label>

                        <div class="input-group">
                            <input type="date" name="tgl_lahir" class="form-control" value="<?php echo $user->tgl_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Foto</label><br/>
                        <img src="<?php echo base_url();?>images/user/<?php echo $user->image;?>" width="50%">
                        <input type="hidden" name="old_image" value="<?php echo $user->image;?>">
                        <input type="hidden" name="id_user" value="<?php echo $user->id_user;?>">
                        <div class="input-group">
                            <input type="file" name="image" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="input-group-icon mt-10">
                        <label>Alamat</label>

                        <div class="input-group">
                            <textarea name="alamat" class="form-control" type="text"><?php echo $user->alamat?></textarea>
                            <!-- <input type="text" name="alamat" class="form-control"> -->
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
                    <button type="submit" class="genric-btn success arrow medium">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach ?>

<!-- delete un -->
<?php foreach ($nilai_uns as $nilai_un):?>
<div class="modal modal-warning fade modal_delete_un" id="delete_un<?php echo $nilai_un->id_un?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" enctype="multipart/form-data" id="form-delete-un">
                <input type="hidden" name="id_un" value="<?php echo $nilai_un->id_un?>">
                <div class="modal-header">
                    <h4>Hapus Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
                </div>
                <div class="modal-body">
                    Apakah anda yakin untuk menghapus data ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Tidak</button>
                    <button id="<?php echo $nilai_un->id_un?>" class="genric-btn success arrow medium delete_un">Ya</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach;?> -->

<!-- Edit -->
<?php foreach ($nilai_uns as $nilai_un):?>
<div class="modal modal-warning fade modal_edit_un" id="edit_un<?php echo $nilai_un->id_un?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" enctype="multipart/form-data" id="form-edit-un">
                <input type="hidden" name="id_un" value="<?php echo $nilai_un->id_un?>">
                <div class="modal-header">
                    <h1>Edit Data</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <!-- <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4> -->
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                            <input type="text" name="nilai_un" placeholder="Nilai" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = 'Nilai'" value="<?php echo $nilai_un->nilai_un;?>"required class="single-input">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger arrow medium" data-dismiss="modal">Close</button>
                    <button class="genric-btn success arrow medium edit_un">Save Changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach;?> -->