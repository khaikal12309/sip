<div class="bradcam_area breadcam_bg overlay2">
	<h3>Data Penilaian <?php echo $this->session->userdata("name_user");?></h3>
</div>
<!-- bradcam_area_end -->

<!-- popular_courses_start -->
<div class="popular_courses">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="box-header">
					<a class="genric-btn primary medium" href="#" id="btn-nilai">Data Nilai</a>
					<a class="genric-btn info-border medium" id="btn-na" href="#">Nilai Akhir</a>
					<a class="genric-btn warning-border medium" id="btn-nr" href="#">Nilai Raport</a>
					<a class="genric-btn success-border medium" href="#" id="btn-un">Nilai UN</a>
					<br/><hr/>
				</div>
				<div class="box">
					<div class="box-body" id="data-table">
						<a class="btn btn-danger" href="<?php echo base_url('index.php/siswa/SiswaNilai/cetak_nilai')?>"></i> Cetak</a>
						<br/><br/>
						<table id="example1" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Tipe Nilai</th>
									<th>Mata Pelajaran</th>
									<th>Nilai</th>
									<th>Diinput Oleh</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($nilais as $nilai):?> 
									<tr>
										<td><?php echo $no;
										$no++;?></td>
										<td><?php echo $nilai->nama_tipe_nilai;?></td>
										<td><?php echo $nilai->nama_mapel;?></td>
										<td><?php echo $nilai->nilai;?></td>
										<td><?php echo $nilai->input_oleh;?></td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets2/js/vendor/jquery-1.12.4.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(function () {
		$('#example1').DataTable()
		$('#example2').DataTable()
	})
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#btn-nilai").on('click', function() {
			$.ajax({
				url: '<?php echo base_url('index.php/siswa/SiswaNilai/show_nilai');?>',
				dataType: 'json',
				type: 'POST',
				cache: false,
				success: function(json) {
					if (json.code == 1){
						$("#data-table").html(json.data_table);
						$("#btn-nilai").removeClass('genric-btn primary-border medium').addClass('genric-btn primary medium');
						$("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
						$("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
						$("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
					}else{
						alert('error');
					}
				},
				error: function () {
					return false;
				}
			});

			return false;
		});

		$("#btn-na").on('click', function() {
			$.ajax({
				url: '<?php echo base_url('index.php/siswa/SiswaNilai/show_na');?>',
				dataType: 'json',
				type: 'POST',
				cache: false,
				success: function(json) {
					if (json.code == 1){
						$("#data-table").html(json.data_table);
						$("#btn-nilai").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
						$("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
						$("#btn-na").removeClass('genric-btn info-border medium').addClass('genric-btn info medium');
						$("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
					}else{
						alert('error');
					}
				},
				error: function () {
					return false;
				}
			});

			return false;
		});

		$("#btn-nr").on('click', function() {
			$.ajax({
				url: '<?php echo base_url('index.php/siswa/SiswaNilai/show_nr');?>',
				dataType: 'json',
				type: 'POST',
				cache: false,
				success: function(json) {
					if (json.code == 1){
						$("#data-table").html(json.data_table);
						$("#btn-nilai").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
						$("#btn-un").removeClass('genric-btn success medium').addClass('genric-btn success-border medium');
						$("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
						$("#btn-nr").removeClass('genric-btn warning-border medium').addClass('genric-btn warning medium');
					}else{
						alert('error');
					}
				},
				error: function () {
					return false;
				}
			});

			return false;
		});
	});

	$("#btn-un").on('click', function() {
			$.ajax({
				url: '<?php echo base_url('index.php/siswa/SiswaNilai/show_un');?>',
				dataType: 'json',
				type: 'POST',
				cache: false,
				success: function(json) {
					if (json.code == 1){
						$("#data-table").html(json.data_table);
						$("#btn-nilai").removeClass('genric-btn primary medium').addClass('genric-btn primary-border medium');
						$("#btn-un").removeClass('genric-btn success-border medium').addClass('genric-btn success medium');
						$("#btn-na").removeClass('genric-btn info medium').addClass('genric-btn info-border medium');
						$("#btn-nr").removeClass('genric-btn warning medium').addClass('genric-btn warning-border medium');
					}else{
						alert('error');
					}
				},
				error: function () {
					return false;
				}
			});

			return false;
		});
</script>
