<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Penilaian
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-pencil"></i> Penilaian</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-info" href="#add_nilai" data-toggle="modal"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                    </div>
                    <div class="box-body"> 
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tugas" data-toggle="tab">Tugas</a></li>
                                <li><a href="#uts" data-toggle="tab">UTS</a></li>
                                <li><a href="#uas" data-toggle="tab">UAS</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tugas">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Siswa</th>
                                                <th>Mata Pelajaran</th>
                                                <th>Nilai</th>
                                                <th>Action</th>
                                                <th>Diinput Oleh</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($nilais as $nilai):
                                                if ($nilai->nama_tipe_nilai == "Tugas"){
                                                if ($this->session->flashdata('id_nilai'))
                                                {
                                                    if ($nilai->id_nilai == $this->session->flashdata('id_nilai'))
                                                    {
                                                        ?> 
                                                        <tr>
                                                            <td><?php echo $no;
                                                            $no++;?></td>
                                                            <td><?php echo $nilai->name_user;?></td>
                                                            <td><?php echo $nilai->nama_mapel;?></td>
                                                            <td><?php echo $nilai->nilai;?></td>
                                                            <td>
                                                                <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                                <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                            </td>
                                                            <td><?php echo $nilai->input_oleh;?></td>
                                                        </tr>
                                                        <?php 
                                                    } 
                                                } else{
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $no;
                                                        $no++;?></td>
                                                        <td><?php echo $nilai->name_user;?></td>
                                                        <td><?php echo $nilai->nama_mapel;?></td>
                                                        <td><?php echo $nilai->nilai;?></td>
                                                        <td>
                                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                        </td>
                                                        <td><?php echo $nilai->input_oleh;?></td>
                                                    </tr>
                                                <?php }} ?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="uts">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Siswa</th>
                                                <th>Mata Pelajaran</th>
                                                <th>Nilai</th>
                                                <th>Action</th>
                                                <th>Diinput Oleh</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($nilais as $nilai):
                                                if ($nilai->nama_tipe_nilai == "UTS"){
                                                if ($this->session->flashdata('id_nilai'))
                                                {
                                                    if ($nilai->id_nilai == $this->session->flashdata('id_nilai'))
                                                    {
                                                        ?> 
                                                        <tr>
                                                            <td><?php echo $no;
                                                            $no++;?></td>
                                                            <td><?php echo $nilai->name_user;?></td>
                                                            <td><?php echo $nilai->nama_mapel;?></td>
                                                            <td><?php echo $nilai->nilai;?></td>
                                                            <td>
                                                                <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                                <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                            </td>
                                                            <td><?php echo $nilai->input_oleh;?></td>
                                                        </tr>
                                                        <?php 
                                                    } 
                                                } else{
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $no;
                                                        $no++;?></td>
                                                        <td><?php echo $nilai->name_user;?></td>
                                                        <td><?php echo $nilai->nama_mapel;?></td>
                                                        <td><?php echo $nilai->nilai;?></td>
                                                        <td>
                                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                        </td>
                                                        <td><?php echo $nilai->input_oleh;?></td>
                                                    </tr>
                                                <?php }} ?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="uas">
                                   <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Siswa</th>
                                                <th>Mata Pelajaran</th>
                                                <th>Nilai</th>
                                                <th>Action</th>
                                                <th>Diinput Oleh</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($nilais as $nilai):
                                                if ($nilai->nama_tipe_nilai == "UAS"){
                                                if ($this->session->flashdata('id_nilai'))
                                                {
                                                    if ($nilai->id_nilai == $this->session->flashdata('id_nilai'))
                                                    {
                                                        ?> 
                                                        <tr>
                                                            <td><?php echo $no;
                                                            $no++;?></td>
                                                            <td><?php echo $nilai->name_user;?></td>
                                                            <td><?php echo $nilai->nama_mapel;?></td>
                                                            <td><?php echo $nilai->nilai;?></td>
                                                            <td>
                                                                <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                                <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                            </td>
                                                            <td><?php echo $nilai->input_oleh;?></td>
                                                        </tr>
                                                        <?php 
                                                    } 
                                                } else{
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $no;
                                                        $no++;?></td>
                                                        <td><?php echo $nilai->name_user;?></td>
                                                        <td><?php echo $nilai->nama_mapel;?></td>
                                                        <td><?php echo $nilai->nilai;?></td>
                                                        <td>
                                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $nilai->id_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/nilai/delete/".$nilai->id_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data Nilai: <?php echo $nilai->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                                        </td>
                                                        <td><?php echo $nilai->input_oleh;?></td>
                                                    </tr>
                                                <?php }} ?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- Modal Add -->
        <div class="modal modal-success fade" id="add_nilai">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url();?>index.php/admin/nilai/do_add" enctype="multipart/form-data">
                        <input type="hidden" name="input_id" value="<?php echo $this->session->userdata('id_user');?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Tambah Data Penilaian</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nama Siswa</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select class="form-control" name="siswa_id">
                                            <option selected="" value="#">--Pilih Siswa--</option>
                                            <?php foreach($siswas as $s):?>
                                                <option value="<?php echo $s->id_user?>"><?php echo $s->name_user?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Mata Pelajaran</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                        <select class="form-control" name="mapel_id">
                                            <option selected="" value="#">--Pilih Mata Pelajaran--</option>
                                            <?php foreach($mapels as $m):?>
                                                <option value="<?php echo $m->id_mapel?>"><?php echo $m->nama_mapel?> - <?php echo $m->tingkat?> - <?php echo $m->jurusan?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group">
                                    <label>Tipe Nilai</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-leaf"></i>
                                        </div>
                                        <select class="form-control" name="tipe_id">
                                            <option selected="" value="#">--Pilih Tipe Nilai--</option>
                                            <?php foreach($tipe_nilais as $tn):?>
                                                <option value="<?php echo $tn->id_tipe_nilai?>"><?php echo $tn->nama_tipe_nilai?> | <?php echo $tn->bobot?>%</option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <div class="form-group">
                                    <label>Nilai</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                                        <input type="text" name="nilai" placeholder="Nilai" class="form-control">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline">Save changes</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <?php foreach($nilais as $nilai):?>
                <!-- Modal Edit -->
                <div class="modal modal-success fade" id="edit<?php echo $nilai->id_nilai;?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form method="POST" action="<?php echo base_url();?>index.php/admin/nilai/do_edit" enctype="multipart/form-data">
                                <input type="hidden" name="id_nilai" value="<?php echo $nilai->id_nilai;?>">
                                <input type="hidden" name="input_id" value="<?php echo $this->session->userdata('id_user');?>">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Edit Data Penilaian <?php echo $nilai->name_user;?>;</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Nama Siswa</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <select class="form-control" name="siswa_id">
                                                    <?php foreach($siswas as $s):?>
                                                        <option value="<?php echo $s->id_user?>" <?php if($s->id_user == $nilai->user_id){echo "selected";}?>><?php echo $s->name_user?></option>
                                                    <?php endforeach?>
                                                </select>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="form-group">
                                            <label>Mata Pelajaran</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-book"></i>
                                                </div>
                                                <select class="form-control" name="mapel_id">
                                                    <?php foreach($mapels as $m):?>
                                                        <option value="<?php echo $m->id_mapel?>" <?php if($m->id_mapel == $nilai->mapel_id){echo "selected";}?>><?php echo $m->nama_mapel?> - <?php echo $m->tingkat?> - <?php echo $m->jurusan?></option>
                                                    <?php endforeach?>
                                                </select>
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                        <div class="form-group">
                                            <label>Tipe Nilai</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-leaf"></i>
                                                </div>
                                                <select class="form-control" name="tipe_id">
                                                    <?php foreach($tipe_nilais as $tn):?>
                                                        <option value="<?php echo $tn->id_tipe_nilai?>" <?php if($tn->id_tipe_nilai == $nilai->tipe_id){echo "selected";}?>><?php echo $tn->nama_tipe_nilai?> | <?php echo $tn->bobot?>%</option>
                                                    <?php endforeach?>
                                                </select>
                                            </div>
                                            <!-- /.input group -->
                                        </div>


                                        <div class="form-group">
                                            <label>Nilai</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-pencil"></i>
                                                </div>
                                                <input type="text" name="nilai" placeholder="Nilai" class="form-control" value="<?php echo $nilai->nilai;?>">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline">Save changes</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                <?php endforeach;?>

                <script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
                <script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
                <script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                <script>
                    $(function () {
                        $('#example1').DataTable()
                        $('#example2').DataTable({
                            'paging'      : true,
                            'lengthChange': false,
                            'searching'   : false,
                            'ordering'    : true,
                            'info'        : true,
                            'autoWidth'   : false
                        })
                    })
                </script>