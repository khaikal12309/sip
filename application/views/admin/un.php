<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Nilai Ujian Nasional
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-book"></i> Data Nilai Ujian Nasional</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert" id="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert" id="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-info" href="#add_kelas" data-toggle="modal"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                        <a class="btn btn-success" href="<?php echo base_url();?>index.php/admin/Un/export_excel"><i class="fa fa-book"></i> Cetak</a>
                    </div> 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Nilai Ujian Nasional</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($uns as $un):?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $un->name_user;?></td>
                                        <td><?php echo $un->nama_mapel;?></td>
                                        <td><?php echo $un->nilai_un;?></td>
                                        <td>
                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $un->id_un?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/un/delete/".$un->id_un?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data: <?php echo $un->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<!-- Modal Add -->
<div class="modal modal-success fade" id="add_kelas">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/Un/do_add" enctype="multipart/form-data">
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tambah Data Nilai Ujian Nasional</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Siswa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <select class="form-control" name="siswa_id">
                                <option selected="" value="#">--Pilih Siswa--</option>
                                <?php foreach($siswas as $s):?>
                                    <option value="<?php echo $s->id_user?>"><?php echo $s->name_user?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Mata Pelajaran</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-book"></i>
                            </div>
                            <select class="form-control" name="mapel_id">
                                <option selected="" value="#">--Pilih Mata Pelajaran--</option>
                                <?php foreach($mapels as $m):?>
                                    <option value="<?php echo $m->id_mapel?>"><?php echo $m->nama_mapel?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nilai Ujian Nasional</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="nilai_un" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<?php foreach ($uns as $un):?>
    <div class="modal modal-success fade" id="edit<?php echo $un->id_un;?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="<?php echo base_url();?>index.php/admin/Un/do_add" enctype="multipart/form-data">
                    <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Edit Data Nilai Ujian Nasional <?php echo $un->name_user;?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Siswa</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select class="form-control" name="siswa_id">
                                    <option selected="" value="#">--Pilih Siswa--</option>
                                    <?php foreach($siswas as $s):?>
                                        <option value="<?php echo $s->id_user?>" <?php if ($s->id_user == $un->siswa_id){echo "selected";}?>><?php echo $s->name_user?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Mata Pelajaran</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <select class="form-control" name="mapel_id">
                                    <option selected="" value="#">--Pilih Mata Pelajaran--</option>
                                    <?php foreach($mapels as $m):?>
                                        <option value="<?php echo $m->id_mapel?>" <?php if ($m->id_mapel == $un->mapel_id){echo "selected";}?>><?php echo $m->nama_mapel?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nilai Ujian Nasional</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-leaf"></i>
                                </div>
                                <input type="text" name="nilai_un" class="form-control" value="<?php echo $un->nilai_un;?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>