<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="preloader" style="position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background-color: #fff;
  display:none;">
      <div class="loading" style="position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%,-50%);
  font: 14px arial;">
        <img src="<?php echo base_url()?>images/pre.gif" width="80">
        <p>Harap Tunggu</p>
      </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Nilai Akhir
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-book"></i> Data Nilai Akhir</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert" id="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert" id="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
                <div class="" role="alert" id="alert" style="display: none">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a class="btn btn-warning" href="<?php echo base_url();?>index.php/admin/NilaiAkhir/do_update"><i class="fa fa-refresh"></i> Update Data</a>

                        <button class="btn btn-success" id="generate_raport"><i class="fa fa-book"></i> Generate Raport</button>
                    </div> 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Tipe Penilaian</th>
                                    <th>Nilai Akhir</th>
                                    <th>Last Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($nilai_akhirs as $nilai_akhir):?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $nilai_akhir->name_user;?></td>
                                        <td><?php echo $nilai_akhir->name_mapel;?></td>
                                        <td><?php echo $nilai_akhir->nama_tipe_nilai;?></td>
                                        <td><?php echo $nilai_akhir->nilai_akhir;?></td>
                                        <td>
                                            <?php echo $nilai_akhir->last_update;?></td>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script>
$(document).ready(function(){
 
    $("#generate_raport").on('click', function () {
        
        $.ajax({
            url: '<?php echo base_url('index.php/admin/Raport/generate_raport');?>',
            dataType: 'json',
            type: 'POST',
            cache: false,
            success: function(json) {
                $(".preloader").fadeIn('slow');
                $(".preloader").fadeOut('slow', function(){
                    document.getElementById('alert').style.display = 'block';
                    $("#alert").html(json.msg).addClass("alert alert-success");
                });
            },
            error: function () {
                $(".preloader").fadeIn('slow');
                $(".preloader").fadeOut('slow', function(){
                    document.getElementById('alert').style.display = 'block';
                    $("#alert").html(json.msg).addClass("alert alert-danger");
                });
            }
        });
        return false;
    });
    // $(".preloader").fadeIn('slow');
    // 
})
</script>