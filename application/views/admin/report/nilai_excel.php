<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<table>
	<thead>
        <tr>
            <th>No</th>
            <th>Siswa</th>
            <th>Tipe Nilai</th>
            <th>Mata Pelajaran</th>
            <th>Nilai</th>
            <th>Diinput Oleh</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($nilais as $nilai):
        ?> 
        <tr>
            <td><?php echo $no;
            $no++;?></td>
            <td><?php echo $nilai->name_user;?></td>
            <td><?php echo $nilai->nama_tipe_nilai;?></td>
            <td><?php echo $nilai->nama_mapel;?></td>
            <td><?php echo $nilai->nilai;?></td>
            <td><?php echo $nilai->input_oleh;?></td>
        </tr>
        <?php endforeach?>
    </tbody>
</table>

</body>
</html>