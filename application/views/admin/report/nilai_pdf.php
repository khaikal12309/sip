<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
	<title></title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<center><h3>Data Nilai</h3></center><br/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-stripped">
				<tr>
					<th>Nama</th>
					<td><?php echo $siswa->name_user;?></td>

					<th>Kelas</th>
					<td><?php echo $siswa->name_kelas;?></td>
				</tr>

				<tr>
					<th>Nomor Induk</th>
					<td><?php echo $siswa->ni;?></td>

					<th>Tempat, Tanggal Lahir</th>
					<td><?php echo $siswa->tpt_lahir;?>, <?php echo $siswa->tgl_lahir;?></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-stripped table-bordered">
				<thead>
			        <tr>
			            <th>No</th>
			            <th>Siswa</th>
			            <th>Tipe Nilai</th>
			            <th>Mata Pelajaran</th>
			            <th>Nilai</th>
			        </tr>
			    </thead>
			    <tbody>
			        <?php
			        $no = 1;
			        foreach ($nilais as $nilai):
			        ?> 
			        <tr>
			            <td><?php echo $no;
			            $no++;?></td>
			            <td><?php echo $nilai->name_user;?></td>
			            <td><?php echo $nilai->nama_tipe_nilai;?></td>
			            <td><?php echo $nilai->nama_mapel;?></td>
			            <td><?php echo $nilai->nilai;?></td>
			        </tr>
			        <?php endforeach?>
			    </tbody>
			</table>
		</div>
	</div>
</div>


</body>
</html>