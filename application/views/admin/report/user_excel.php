<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<table>
	<thead>
		<tr>
			<th>Nomor Induk</th>
			<th>Nama Siswa</th>
			<th>Kelas</th>
			<th>Tempat, Tanggal Lahir</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user):?> 
			<tr>
				<td><?php echo $user->ni;?></td>
				<td><?php echo $user->name_user;?></td>
				<td><?php echo $user->name_kelas;?></td>
				<td><?php echo $user->tpt_lahir;?>, <?php echo $user->tgl_lahir;?></td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>

</body>
</html>