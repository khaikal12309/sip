<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<table>
	<thead>
        <tr>
            <th>No</th>
            <th>Nama Siswa</th>
            <th>Mata Pelajaran</th>
            <th>Tipe Penilaian</th>
            <th>Nilai Akhir</th>
            <th>Last Update</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($nilai_akhirs as $na):
        ?> 
        <tr>
            <td><?php echo $no;
            $no++;?></td>
            <td><?php echo $na->name_user;?></td>
            <td><?php echo $na->name_mapel;?></td>
            <td><?php echo $na->nama_tipe_nilai;?></td>
            <td><?php echo $na->nilai_akhir;?></td>
            <td><?php echo $na->last_update;?></td>
        </tr>
        <?php endforeach?>
    </tbody>
</table>

</body>
</html>