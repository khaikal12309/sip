<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Siswa</th>
				<th>Mata Pelajaran</th>
				<th>Nilai Ujian Nasional</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			foreach ($uns as $un):?> 
			<tr>
				<td><?php echo $no;
					$no++;?></td>
					<td><?php echo $un->name_user;?></td>
					<td><?php echo $un->nama_mapel;?></td>
					<td><?php echo $un->nilai_un;?></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>

	</body>
	</html>