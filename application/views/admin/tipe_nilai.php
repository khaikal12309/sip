<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Tipe Nilai
            <small>Master Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
            <li class="active">Data Tipe Nilai</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-info" href="#add_mapel" data-toggle="modal" <?php if($total_bobot == 100){echo "disabled";} ?>><i class="fa fa-plus-circle"></i> Tambah Data</button>
                        <?php if($total_bobot == 100){echo "<span style='color:red'>*Bobot telah melebihi batas</span>";} ?>
                    </div> 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tipe Nilai</th>
                                    <th>Bobot (%)</th>
                                    <th>Deskripsi</th>
                                    <th>Multi Input?</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($tipe_nilais as $tipe_nilai):?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $tipe_nilai->nama_tipe_nilai;?></td>
                                        <td><?php echo $tipe_nilai->bobot;?></td>
                                        <td><?php echo $tipe_nilai->deskripsi;?></td>
                                        <td>
                                            <?php if ($tipe_nilai->is_multi_input){
                                                echo "Ya";
                                            } else{
                                                echo "Tidak";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $tipe_nilai->id_tipe_nilai?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/tipe_nilai/delete/".$tipe_nilai->id_tipe_nilai?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data: <?php echo $tipe_nilai->nama_tipe_nilai;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal Add -->
<div class="modal modal-success fade" id="add_mapel">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="form-tambah-data" enctype="multipart/form-data">
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tambah Data Tipe Nilai</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Tipe Nilai</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="nama_tipe_nilai" id="nama_tipe_nilai" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group" id="form-bobot">
                        <label>Bobot</label>
                        <div class="input-group">
                            <input type="number" name="bobot" placeholder="Bobot" id="bobot" class="form-control"></input>
                            <div class="input-group-addon">
                                %
                            </div>
                        </div>
                        <p>Sisa bobot <b><?php echo 100-$total_bobot;?> %</b></p>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Multi Input?</label>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="is_multi_input" value="1">
                          <label class="form-check-label" for="inlineRadio1">Ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="is_multi_input" value="0">
                          <label class="form-check-label" for="inlineRadio2">Tidak</label>
                        </div>
                            <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Deskripsi</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-book"></i>
                            </div>
                            <textarea name="deskripsi" placeholder="Deskripsi" class="form-control"></textarea>
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline" id="btn-tambah-data">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>


<?php foreach ($tipe_nilais as $t):?>
<!-- Modal Edit -->
<div class="modal modal-success fade" id="edit<?php echo $t->id_tipe_nilai?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('index.php/admin/tipe_nilai/do_edit');?>">
                <input type="hidden" name="id_tipe_nilai" value="<?php echo $t->id_tipe_nilai?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Data Tipe Nilai <?php echo $t->nama_tipe_nilai?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Tipe Nilai</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="nama_tipe_nilai" id="nama_tipe_nilai_edit" value="<?php echo $t->nama_tipe_nilai?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group" id="form-bobot-edit">
                        <label>Bobot</label>
                        <div class="input-group">
                            <input type="hidden" name="bobot_old" value="<?php echo $t->bobot?>"></input>
                            <input type="number" name="bobot" placeholder="Bobot" id="bobot-edit" class="form-control" value="<?php echo $t->bobot?>"></input>
                            <div class="input-group-addon">
                                %
                            </div>
                        </div>
                        <p>Sisa bobot <b><?php echo 100-$total_bobot;?> %</b></p>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                            <label>Multi Input??</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="is_multi_input" value="1" <?php if ($t->is_multi_input == True){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio1">Ya</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="is_multi_input" value="0" <?php if ($t->is_multi_input == False){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio2">Tidak</label>
                            </div>
                            <!-- /.input group -->
                        </div>

                    <div class="form-group">
                        <label>Deskripsi</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-book"></i>
                            </div>
                            <textarea name="deskripsi" placeholder="Deskripsi" class="form-control"><?php echo $t->deskripsi?></textarea>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline" id="btn-edit-data">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>

<?php endforeach ?>


<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  $(document).ready(function () {

        $("#btn-tambah-data").on('click', function () {
            $.ajax({
                url: '<?php echo base_url('index.php/admin/tipe_nilai/do_add');?>',
                data: $("#form-tambah-data").serialize(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    alert(json.msg);
                    window.location = '<?php echo base_url('index.php/admin/tipe_nilai');?>';
                },
                error: function () {
                    alert(json.msg);
                }
            });

            return false;
        });

        // $("#btn-edit-data").on('click', function () {
        //     $.ajax({
        //         url: '<?php echo base_url('index.php/admin/tipe_nilai/do_edit');?>',
        //         data: $("#form-edit-data").serialize(),
        //         dataType: 'json',
        //         type: 'POST', 
        //         cache: false,
        //         success: function(json) {
        //             alert(json.msg);
        //             // window.location = '<?php echo base_url('index.php/admin/tipe_nilai');?>';
        //         },
        //         error: function () {
        //             alert(json.msg);
        //         }
        //     });

        //     return false;
        // });

        $("#nama_tipe_nilai").on('change', function () {
            var nama_tipe_nilai = $(this).val();
            if (nama_tipe_nilai == "UN"){
                $("#bobot").val("");
                document.getElementById('form-bobot').style.display = 'none';
                alert("Tipe nilai UN tidak perlu menambahkan bobot");
            }
            
        });

        $("#nama_tipe_nilai_edit").on('change', function () {
            var nama_tipe_nilai = $(this).val();
            if (nama_tipe_nilai == "UN"){
                $("#bobot-edit").val("");
                document.getElementById('form-bobot-edit').style.display = 'none';
                alert("Tipe nilai UN tidak perlu menambahkan bobot");
            }
            
        });

    });



</script>

<script>
    
</script>