<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Siswa
            <small>Master Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
            <li class="active">Data Siswa</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <button class="btn btn-info" href="#add_user" data-toggle="modal"><i class="fa fa-plus-circle"></i> Tambah Data</button>

                        <a href="<?php echo base_url()?>index.php/admin/user/export_excel" class="btn btn-success"><i class="fa fa-print"> Excel</i></a>
                        <a href="<?php echo base_url()?>index.php/admin/user/export_pdf" class="btn btn-danger"><i class="fa fa-print"> Pdf</i></a>

                    </div> 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nomor Induk</th>
                                    <th>Nama Siswa</th>
                                    <th>Kelas</th>
                                    <th>Tempat, Tanggal Lahir</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user):?> 
                                    <tr>
                                        <td><?php echo $user->ni;?></td>
                                        <td><?php echo $user->name_user;?></td>
                                        <td><?php echo $user->name_kelas;?></td>
                                        <td><?php echo $user->tpt_lahir;?>, <?php echo $user->tgl_lahir;?></td>
                                        <td>
                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $user->id_user?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/user/delete/".$user->id_user?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data: <?php echo $user->name_user;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                            <a class="btn btn-info btn-sm" href="#detail<?php echo $user->id_user?>" data-toggle="modal"><i class="fa fa-eye"></i> Detail</a>
                                            <a class="btn btn-default btn-sm" href="#pass<?php echo $user->id_user?>" data-toggle="modal"><i class="fa fa-key"></i> Reset</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<!-- Modal Add -->
<div class="modal modal-success fade" id="add_user">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/user/do_add" enctype="multipart/form-data">
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tambah Data Siswa</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-registered"></i>
                            </div>
                            <input type="text" name="username" class="form-control"><br/>
                        </div>
                            <b>*Username untuk login siswa (default password = 123456)</b>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nomor Induk</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="ni" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-at"></i>
                            </div>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nama Siswa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <input type="text" name="name_user" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Phone</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <input type="text" name="phone" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Kelas</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-map-pin"></i>
                            </div>
                            <select class="form-control" name="kelas_id">
                                <option selected="" value="#">--Pilih Kelas--</option>
                                <?php foreach($kelas as $k):?>
                                    <option value="<?php echo $k->id_kelas?>"><?php echo $k->name_kelas?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tempat Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map"></i>
                            </div>
                            <input type="text" name="tpt_lahir" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tanggal Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-calendar"></i>
                            </div>
                            <input type="date" name="tgl_lahir" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Foto</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-image"></i>
                            </div>
                            <input type="file" name="image" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-location-arrow"></i>
                            </div>
                            <textarea name="alamat" class="form-control"></textarea>
                            <!-- <input type="text" name="alamat" class="form-control"> -->
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>

<?php foreach($users as $user):?>
<!-- Modal Edit -->
<div class="modal modal-warning fade" id="edit<?php echo $user->id_user?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/user/do_edit" enctype="multipart/form-data">
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Data <i><?php echo $user->name_user;?></i></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-registered"></i>
                            </div>
                            <input type="text" name="username" class="form-control" value="<?php echo $user->username?>"><br/>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nomor Induk</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="ni" class="form-control" value="<?php echo $user->ni?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-at"></i>
                            </div>
                            <input type="email" name="email" class="form-control" value="<?php echo $user->email?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nama Siswa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <input type="text" name="name_user" class="form-control" value="<?php echo $user->name_user?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Phone</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <input type="text" name="phone" class="form-control" value="<?php echo $user->phone?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Kelas</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-map-pin"></i>
                            </div>
                            <select class="form-control" name="kelas_id">
                                <option selected="" value="#">--Pilih Kelas--</option>
                                <?php foreach($kelas as $k):?>
                                    <option value="<?php echo $k->id_kelas?>" <?php if ($k->id_kelas == $user->kelas_id){echo "selected";}?>><?php echo $k->name_kelas?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tempat Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map"></i>
                            </div>
                            <input type="text" name="tpt_lahir" class="form-control" value="<?php echo $user->tpt_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tanggal Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-calendar"></i>
                            </div>
                            <input type="date" name="tgl_lahir" class="form-control" value="<?php echo $user->tgl_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Foto</label><br/>
                        <img src="<?php echo base_url();?>images/user/<?php echo $user->image;?>" width="50%">
                        <input type="hidden" name="old_image" value="<?php echo $user->image;?>">
                        <input type="hidden" name="id_user" value="<?php echo $user->id_user;?>">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-image"></i>
                            </div>
                            <input type="file" name="image" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-location-arrow"></i>
                            </div>
                            <textarea name="alamat" class="form-control" type="text"><?php echo $user->username?></textarea>
                            <!-- <input type="text" name="alamat" class="form-control"> -->
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach ?>


<?php foreach($users as $user):?>
<!-- Modal Detai -->
<div class="modal modal-info fade" id="detail<?php echo $user->id_user?>">
    <div class="modal-dialog">
        <div class="modal-content">
           
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Detail Data <i><?php echo $user->name_user;?></i></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-registered"></i>
                            </div>

                            <input type="text" readonly="" class="form-control" value="<?php echo $user->username?>"><br/>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nomor Induk</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" readonly="" class="form-control" value="<?php echo $user->ni?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-at"></i>
                            </div>
                            <input type="email" class="form-control" value="<?php echo $user->email?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Nama Siswa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <input type="text" readonly="" class="form-control" value="<?php echo $user->name_user?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Phone</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <input type="text" readonly="" class="form-control" value="<?php echo $user->phone?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Kelas</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-map-pin"></i>
                            </div>
                            <input type="text" readonly="" class="form-control" value="<?php echo $user->name_kelas?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tempat Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map"></i>
                            </div>
                            <input type="text" readonly="" class="form-control" value="<?php echo $user->tpt_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Tanggal Lahir</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-calendar"></i>
                            </div>
                            <input type="date" class="form-control" value="<?php echo $user->tgl_lahir?>">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Foto</label><br/>
                        <img src="<?php echo base_url();?>images/user/<?php echo $user->image;?>" width="50%">
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-location-arrow"></i>
                            </div>
                            <textarea class="form-control" type="text"><?php echo $user->username?></textarea>
                            <!-- <input type="text" readonly="" name="alamat" class="form-control"> -->
                        </div>
                        <!-- /.input group -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                </div>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach ?>

<!-- Change Password -->
<?php foreach ($users as $user):?>
<div class="modal modal-warning fade" id="pass<?php echo $user->id_user?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/user/reset_password">
                <input type="hidden" name="id_user" value="<?php echo $user->id_user?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Reset Password <?php echo $user->name_user?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Password</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Confirm Password</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </div>
                            <input type="password" name="passconf" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<?php endforeach ?>

<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/printPreview.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>