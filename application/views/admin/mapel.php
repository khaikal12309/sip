<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Mata Pelajaran
            <small>Master Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
            <li class="active">Data Mapel</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('error')):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif ?>
                <?php if($this->session->flashdata('success')):?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success');?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-info" href="#add_mapel" data-toggle="modal"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                    </div> 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>KD Mapel</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Tingkat</th>
                                    <th>Jurusan</th>
                                    <th>Guru</th>
                                    <th>Mapel UN?</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($mapels as $mapel):?> 
                                    <tr>
                                        <td><?php echo $no;
                                        $no++;?></td>
                                        <td><?php echo $mapel->kd_mapel;?></td>
                                        <td><?php echo $mapel->nama_mapel;?></td>
                                        <td><?php echo $mapel->tingkat;?></td>
                                        <td><?php echo $mapel->jurusan;?></td>
                                        <td><?php echo $mapel->name_user;?></td>
                                        <td>
                                            <?php if ($mapel->is_un){
                                                echo "Ya";
                                            }else{
                                                echo "Tidak";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-sm" href="#edit<?php echo $mapel->id_mapel?>" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url()."index.php/admin/mapel/delete/".$mapel->id_mapel?>" onClick="return confirm('Apakah anda yakin ingin menghapus Data: <?php echo $mapel->nama_mapel;?>');"><i class="fa fa-trash-o"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal Add -->
<div class="modal modal-success fade" id="add_mapel">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/mapel/do_add" enctype="multipart/form-data">
                <!-- <input type="hidden" name="id_user" value="<?php echo $user->id_user?>"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Tambah Data Mata Pelajaran</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Mata Pelajaran</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <input type="text" name="nama_mapel" class="form-control">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group">
                        <label>Tingkat</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <select class="form-control" name="tingkat">
                                <option selected="" value="#">--Pilih Tingkat--</option>
                                    <option value="X">X</option>
                                    <option value="XI">XI</option>
                                    <option value="XII">XII</option>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Guru</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <select class="form-control" name="guru_id">
                                <option selected="" value="#">--Pilih Guru--</option>
                                <?php foreach($gurus as $g):?>
                                    <option value="<?php echo $g->id_user?>"><?php echo $g->name_user?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Jurusan</label>

                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="jurusan" value="IPA">
                          <label class="form-check-label" for="inlineRadio1">IPA</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="jurusan" value="IPS">
                          <label class="form-check-label" for="inlineRadio2">IPS</label>
                        </div>
                        <!-- /.input group -->
                    </div>
                                        
                    <div class="form-group">
                        <label>Mapel UN?</label>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="is_un" value="1">
                          <label class="form-check-label" for="inlineRadio1">Ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="is_un" value="0">
                          <label class="form-check-label" for="inlineRadio2">Tidak</label>
                        </div>
                            <!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline">Save changes</button>
                </div>
            </form>
        </div>
            <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>

<!-- Modal Edit -->
<?php foreach($mapels as $mapel): ?>
    <div class="modal modal-warning fade" id="edit<?php echo $mapel->id_mapel?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="<?php echo base_url();?>index.php/admin/mapel/do_edit" enctype="multipart/form-data">
                    <input type="hidden" name="id_mapel" value="<?php echo $mapel->id_mapel?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Edit Data Mata Pelajaran <?php echo $mapel->nama_mapel?> </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Mata Pelajaran</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-leaf"></i>
                                </div>
                                <input type="text" name="nama_mapel" value="<?php echo $mapel->nama_mapel?>" class="form-control">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Tingkat</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select class="form-control" name="tingkat">
                                    <option value="#" <?php if ($mapel->tingkat == Null){echo "selected";}?>>-- Pilih Tingkat --</option>
                                        <option value="X" <?php if ($mapel->tingkat == "X"){echo "selected";}?>>X</option>
                                        <option value="XI" <?php if ($mapel->tingkat == "XI"){echo "selected";}?>>XI</option>
                                        <option value="XII" <?php if ($mapel->tingkat == "XII"){echo "selected";}?>>XII</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Guru</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select class="form-control" name="guru_id">
                                    <option selected="" value="#">--Pilih Guru--</option>
                                    <?php foreach($gurus as $g):?>
                                        <option value="<?php echo $g->id_user?>" <?php if ($mapel->guru_id == $g->id_user){echo "selected";}?>><?php echo $g->name_user?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <label>Jurusan</label>

                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="jurusan" value="IPA" <?php if ($mapel->jurusan == "IPA"){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio1">IPA</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="jurusan" value="IPS" <?php if ($mapel->jurusan == "IPS"){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio2">IPS</label>
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <label>Mapel UN?</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="is_un" value="1" <?php if ($mapel->is_un == True){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio1">Ya</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="is_un" value="0" <?php if ($mapel->is_un == False){echo "checked";}?>>
                              <label class="form-check-label" for="inlineRadio2">Tidak</label>
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline">Save changes</button>
                    </div>
                </form>
            </div>
                <!-- /.modal-content -->
        </div>
            <!-- /.modal-dialog -->
    </div>
<?php endforeach ?>

<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>