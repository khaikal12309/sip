<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Raport extends CI_Model
{
    private $_table = "tb_raport";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
        return $this->db->query('SELECT r.id_raport as "id_raport", r.siswa_id as "siswa_id", u.name_user AS "name_user", r.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", r.last_update as "last_update", r.nilai AS "nilai" FROM tb_raport AS r LEFT JOIN tb_user AS u on u.id_user = r.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = r.mapel_id')->result();
    }

    public function getMyRaport()
    {
        $guru_id = $this->session->userdata('id_user');
        return $this->db->query('SELECT r.id_raport as "id_raport", r.siswa_id as "siswa_id", u.name_user AS "name_user", r.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", r.last_update as "last_update", r.nilai AS "nilai" FROM tb_raport AS r LEFT JOIN tb_user AS u on u.id_user = r.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = r.mapel_id LEFT JOIN tb_kelas AS k ON k.id_kelas= u.kelas_id WHERE k.guru_id = '.$guru_id.'' )->result();
    }

    public function getRaportBySiswa()
    {
        $siswa_id = $this->session->userdata('id_user');
        return $this->db->query('SELECT r.id_raport as "id_raport", r.siswa_id as "siswa_id", u.name_user AS "name_user", r.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", r.last_update as "last_update", r.nilai AS "nilai" FROM tb_raport AS r LEFT JOIN tb_user AS u on u.id_user = r.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = r.mapel_id LEFT JOIN tb_kelas AS k ON k.id_kelas= u.kelas_id WHERE r.siswa_id = '.$siswa_id.'' )->result();
    }

    public function empty_table()
    {   
        return $this->db->empty_table($this->_table,Null);
    }
}