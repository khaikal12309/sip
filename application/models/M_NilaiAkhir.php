<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_NilaiAkhir extends CI_Model
{
    private $_table = "tb_nilai_akhir";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getNa()
    {
        return $this->db->query('SELECT n.siswa_id,n.mapel_id,n.tipe_id, (SELECT bobot FROM tb_tipe_nilai AS tn WHERE tn.id_tipe_nilai = n.tipe_id)*(AVG(n.nilai))/100 AS n_a FROM tb_nilai AS n GROUP by n.tipe_id,n.siswa_id,n.mapel_id')->result();
    }

    public function getMyNa()
    {
        $guru_id = $this->session->userdata('id_user');
        return $this->db->query('SELECT na.id_nilai_akhir as "id_nilai_akhir", na.siswa_id as "siswa_id", u.name_user AS "name_user", na.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", na.tipe_id as "tipe_id", tn.nama_tipe_nilai AS "nama_tipe_nilai", na.last_update as "last_update", na.nilai_akhir AS "nilai_akhir" FROM tb_nilai_akhir AS na LEFT JOIN tb_user AS u on u.id_user = na.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = na.mapel_id LEFT JOIN tb_tipe_nilai as tn ON tn.id_tipe_nilai = na.tipe_id LEFT JOIN tb_kelas as k ON u.kelas_id = k.id_kelas WHERE k.guru_id = '.$guru_id.'')->result();
    }

    public function getNaBySiswa()
    {
        $siswa_id = $this->session->userdata('id_user');
        return $this->db->query('SELECT na.id_nilai_akhir as "id_nilai_akhir", na.siswa_id as "siswa_id", u.name_user AS "name_user", na.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", na.tipe_id as "tipe_id", tn.nama_tipe_nilai AS "nama_tipe_nilai", na.last_update as "last_update", na.nilai_akhir AS "nilai_akhir" FROM tb_nilai_akhir AS na LEFT JOIN tb_user AS u on u.id_user = na.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = na.mapel_id LEFT JOIN tb_tipe_nilai as tn ON tn.id_tipe_nilai = na.tipe_id LEFT JOIN tb_kelas as k ON u.kelas_id = k.id_kelas WHERE na.siswa_id = '.$siswa_id.'')->result();
    }

    public function getAll()
    {
        return $this->db->query('SELECT na.id_nilai_akhir as "id_nilai_akhir", na.siswa_id as "siswa_id", u.name_user AS "name_user", na.mapel_id AS "mapel_id", m.nama_mapel AS "name_mapel", na.tipe_id as "tipe_id", tn.nama_tipe_nilai AS "nama_tipe_nilai", na.last_update as "last_update", na.nilai_akhir AS "nilai_akhir" FROM tb_nilai_akhir AS na LEFT JOIN tb_user AS u on u.id_user = na.siswa_id LEFT JOIN tb_mapel AS m ON m.id_mapel = na.mapel_id LEFT JOIN tb_tipe_nilai as tn ON tn.id_tipe_nilai = na.tipe_id')->result();
    }

    public function empty_table()
    {   
        return $this->db->empty_table($this->_table,Null);
    }

    public function getDataRaport()
    {
        return $this->db->query('SELECT siswa_id,mapel_id,sum(nilai_akhir) as "total" from tb_nilai_akhir GROUP BY siswa_id, mapel_id')->result();
    }
}