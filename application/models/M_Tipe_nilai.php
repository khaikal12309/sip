<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Tipe_nilai extends CI_Model
{
    private $_table = "tb_tipe_nilai";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getTipenilai()
    {
        $this->db->select('*');
        $this->db->from('tb_tipe_nilai');
        // $this->db->join('tb_kelas', 'tb_kelas.id_kelas = tb_user.kelas_id');
        $this->db->where('bobot !=',Null);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getBobot()
    {
        $bobot_old = 0;
        
        if ($this->input->post('bobot_old') != Null){
            $bobot_old = $this->input->post('bobot_old');
        }

        $bobot = $this->input->post('bobot');
        
        $nilaibobot = $this->db->query("SELECT SUM(BOBOT) as tot_bobot from tb_tipe_nilai")->result();
        
        if($nilaibobot != Null){
            $bobot = $bobot + $nilaibobot[0]->tot_bobot - $bobot_old;
        }
        return $bobot;
    }
    
    public function totalBobot()
    {
        
        $nilaibobot = $this->db->query("SELECT SUM(BOBOT) as tot_bobot from tb_tipe_nilai")->result();
        return $nilaibobot[0]->tot_bobot;;
    }

    public function save()
    {
 
        $post = $this->input->post();

        $this->nama_tipe_nilai = $post["nama_tipe_nilai"];
        $this->bobot = $post["bobot"];
        $this->deskripsi = $post["deskripsi"];
        $this->is_multi_input = $post["is_multi_input"];
        
        return $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id_tipe_nilai = $post["id_tipe_nilai"];
        $this->nama_tipe_nilai = $post["nama_tipe_nilai"];
        $this->bobot = $post["bobot"];
        $this->deskripsi = $post["deskripsi"];
        $this->is_multi_input = $post["is_multi_input"];
        return $this->db->update($this->_table, $this, array('id_tipe_nilai' => $post['id_tipe_nilai']));
    }

    public function delete($id_tipe_nilai)
    {   
        return $this->db->delete($this->_table, array("id_tipe_nilai" => $id_tipe_nilai));
    }

}