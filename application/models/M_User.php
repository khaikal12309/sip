<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model
{
    private $_table = "tb_user";

    public $id_user;
    public $image = "default.png";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('tb_user.state !=',"admin");
        return $this->db->get()->result();
    }

    public function getMe()
    {
        $id_user = $this->session->userdata('id_user');
        return $this->db->query('SELECT u.*,k.* from tb_user as u left join tb_kelas as k on u.kelas_id = k.id_kelas where u.id_user = '.$id_user.'')->row();
    }

    public function getByKelas($kelas_id)
    {
        return $this->db->query('SELECT u.*,k.* from tb_user as u left join tb_kelas as k on u.kelas_id = k.id_kelas where u.kelas_id = '.$kelas_id.' and u.state="siswa"')->result();
    }

    public function getGuru()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('tb_user.state =',"guru");
        return $this->db->get()->result();
    }
    
    public function getSiswa()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->join('tb_kelas', 'tb_kelas.id_kelas = tb_user.kelas_id');
        $this->db->where('tb_user.state =',"siswa");
        // $query = $this->db->get();
        return $this->db->get()->result();
    }
 
    public function getById($id_user)
    {
        return $this->db->get_where($this->_table, ["id_user" => $id_user])->row();
    }

    public function cek()
    {
    	$post = $this->input->post();
        $id_user = $post["id_user"];
        $password = MD5($post['old_password']);
    	return $this->db->get_where($this->_table, ["id_user" => $id_user,"password" => $password])->num_rows(); 
    }

    public function check(){
        $post = $this->input->post();
        $email = $post["email"]; 
        // print_r($email);
        // die();     
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('email',$email);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return TRUE;
            
        } else {
            return FALSE;
        }
    }

    public function save()
    {
        $post = $this->input->post();

        $this->name_user = $post["name_user"];
        $this->username = $post["username"];
        $this->password = MD5('123456');
        $this->alamat = $post["alamat"];
        $this->phone = $post["phone"];
        $this->email = $post["email"];
        $this->kelas_id = $post["kelas_id"];
        $this->ni = $post["ni"];
        $this->state = 'siswa';
        $this->tpt_lahir = $post["tpt_lahir"];
        $this->tgl_lahir = $post["tgl_lahir"];
        $this->image = $this->_uploadImage();
        return $this->db->insert($this->_table, $this);
    }

    public function save_guru()
    {
        $post = $this->input->post();

        $this->name_user = $post["name_user"];
        $this->username = $post["username"];
        $this->password = MD5('123456');
        $this->alamat = $post["alamat"];
        $this->phone = $post["phone"];
        $this->email = $post["email"];
        $this->kelas_id = $post["kelas_id"];
        $this->ni = $post["ni"];
        $this->state = 'guru';
        $this->tpt_lahir = $post["tpt_lahir"];
        $this->tgl_lahir = $post["tgl_lahir"];
        $this->image = $this->_uploadImage();
        return $this->db->insert($this->_table, $this);
    }

    public function update(){
        $post = $this->input->post();
        $this->id_user = $post["id_user"];
        $this->name_user = $post["name_user"];
        $this->alamat = $post["alamat"];
        $this->phone = $post["phone"];
        $this->email = $post["email"];
        $this->kelas_id = $post["kelas_id"];
        $this->ni = $post["ni"];
        // $this->state = 'siswa';
        $this->tpt_lahir = $post["tpt_lahir"];
        $this->tgl_lahir = $post["tgl_lahir"];
        if (!empty($_FILES["image"]["name"])) {
            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
        }
        return $this->db->update($this->_table, $this, array('id_user' => $post['id_user']));
    }

    public function update_pass(){
    	$post = $this->input->post();
    	$data['id_user'] = $post["id_user"];
    	$data['password'] = MD5($post['password']);
    	return $this->db->update($this->_table, $data, array('id_user' => $post['id_user']));
    }

    public function delete($id_user)
    {
        $this->_deleteImage($id_user);
        
        return $this->db->delete($this->_table, array("id_user" => $id_user));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './images/user';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'img_'.$this->name_user;
        $config['overwrite']            = true;
        $config['max_size']             = 4000; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
        return  "default.png";
    }

    private function _deleteImage($id)
    {
        $user = $this->getById($id);
        if ($user->image != "default.jpg") {
            $filename = explode(".", $user->image)[0];
            return array_map('unlink', glob(FCPATH."image/user/$filename.*"));
        }
    }
}