<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Un extends CI_Model
{
    private $_table = "tb_un";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
         $this->db->select('*');
        $this->db->from('tb_un');
        $this->db->join('tb_user', 'tb_un.siswa_id = tb_user.id_user','left');
        $this->db->join('tb_mapel', 'tb_un.mapel_id = tb_mapel.id_mapel','left');
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getUnBySiswa()
    {
        $siswa_id = $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->from('tb_un');
        $this->db->join('tb_user', 'tb_un.siswa_id = tb_user.id_user','left');
        $this->db->join('tb_mapel', 'tb_un.mapel_id = tb_mapel.id_mapel','left');
        $this->db->join('tb_kelas', 'tb_user.kelas_id = tb_kelas.id_kelas','left');
        $this->db->where('tb_un.siswa_id =',$siswa_id);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getByGuru()
    {
        $guru_id = $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->from('tb_un');
        $this->db->join('tb_user', 'tb_un.siswa_id = tb_user.id_user','left');
        $this->db->join('tb_mapel', 'tb_un.mapel_id = tb_mapel.id_mapel','left');
        $this->db->join('tb_kelas', 'tb_user.kelas_id = tb_kelas.id_kelas','left');
        $this->db->where('tb_kelas.guru_id =',$guru_id);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function save()
    {
 
        $post = $this->input->post();
        $cek = $this->db->query('SELECT * FROM tb_un WHERE siswa_id = '.$post["siswa_id"].' and mapel_id='.$post["mapel_id"].'')->row();
        if (count($cek) == 0){
            $this->siswa_id = $post["siswa_id"];
            $this->mapel_id = $post["mapel_id"];
            $this->nilai_un = $post["nilai_un"];
            return $this->db->insert($this->_table, $this);
        } else{
            return False;
        }
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id_un = $post["id_un"];
        $this->nilai_un = $post["nilai_un"];
        return $this->db->update($this->_table, $this, array('id_un' => $post['id_un']));
    }

    public function delete($id_un)
    {   
        return $this->db->delete($this->_table, array("id_un" => $id_un));
    }

}