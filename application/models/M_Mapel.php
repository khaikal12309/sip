<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Mapel extends CI_Model
{
    private $_table = "tb_mapel";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getMapel()
    {
         $this->db->select('*');
        $this->db->from('tb_mapel');
        $this->db->join('tb_user', 'tb_mapel.guru_id = tb_user.id_user','left');
        $this->db->where('tb_user.state =',"guru");
        $this->db->where('tb_mapel.is_active =',True);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getMapelByGuru($guru_id)
    {
         $this->db->select('*');
        $this->db->from('tb_mapel');
        $this->db->where('tb_mapel.is_active =',True);
         $this->db->where('tb_mapel.guru_id =',$guru_id);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getMapelUN()
    {
         $this->db->select('*');
        $this->db->from('tb_mapel');
        $this->db->join('tb_user', 'tb_mapel.guru_id = tb_user.id_user','left');
        $this->db->where('tb_user.state =',"guru");
        $this->db->where('tb_mapel.is_un =',True);
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function get_kd_mapel()
 	{
 		$id_max = $this->db->query('SELECT MAX(id_mapel) FROM tb_mapel limit 1')->result();
 		$max = (int) $id_max + 1;
 		$kd_mapel = "KD-MAPEL-$max";
 		return $kd_mapel;
 	}

    public function save()
    {
 
        $post = $this->input->post();

        $this->nama_mapel = $post["nama_mapel"];
        $this->tingkat = $post["tingkat"];
        $this->kd_mapel = $this->get_kd_mapel();
        $this->jurusan = $post["jurusan"];
        $this->guru_id = $post["guru_id"];
        $this->is_un = $post["is_un"];
        $this->is_active = 1;
        
        return $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id_mapel = $post["id_mapel"];
        $this->nama_mapel = $post["nama_mapel"];
        $this->tingkat = $post["tingkat"];
        $this->kd_mapel = $this->get_kd_mapel();
        $this->jurusan = $post["jurusan"];
        $this->guru_id = $post["guru_id"];
        $this->is_un = $post["is_un"];
        $this->is_active = 1;
        return $this->db->update($this->_table, $this, array('id_mapel' => $post['id_mapel']));
    }

    public function delete($id_mapel)
    {   
        return $this->db->delete($this->_table, array("id_mapel" => $id_mapel));
    }

    public function GetByGuru($kelas_id)
    {
        $guru_id = $this->session->userdata('id_user');
        return $this->db->query('SELECT m.*, k.tingkat, k.jurusan FROM tb_mapel as m, tb_kelas AS k where k.tingkat = m.tingkat and k.jurusan = m.jurusan and m.guru_id = '.$guru_id.' and k.id_kelas = '.$kelas_id.'')->result();
        
    }
}