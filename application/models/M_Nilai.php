        <?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Nilai extends CI_Model
{
    private $_table = "tb_nilai";

    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function getMapel()
    {
        $this->db->select('*');
        $this->db->from('tb_mapel');
        $this->db->join('tb_user', 'tb_mapel.guru_id = tb_user.id_user','left');
        $this->db->where('tb_user.state =',"guru");
        $this->db->where('tb_mapel.is_active =',True);
// $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getAll()
    {
        $data = $this->db->query('SELECT n.id_nilai as "id_nilai", n.nilai as "nilai",u.id_user as "user_id", u.name_user,tn.id_tipe_nilai as "tipe_id",tn.nama_tipe_nilai,m.id_mapel as "mapel_id",m.nama_mapel, n.input_id, (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = m.guru_id) AS "nama_guru", (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = n.input_id) AS "input_oleh" FROM tb_nilai as n LEFT JOIN tb_user as u ON n.siswa_id = u.id_user LEFT JOIN tb_tipe_nilai as tn ON n.tipe_id= tn.id_tipe_nilai LEFT JOIN tb_mapel as m ON n.mapel_id = m.id_mapel')->result();
        return $data;
        
    }

    public function getNilaiByGuru()
    {
        $guru_id = $this->session->userdata('id_user');
        $data = $this->db->query('SELECT n.id_nilai as "id_nilai", n.nilai as "nilai",u.id_user as "user_id", u.name_user,tn.id_tipe_nilai as "tipe_id",tn.nama_tipe_nilai,m.id_mapel as "mapel_id",m.nama_mapel, n.input_id, (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = m.guru_id) AS "nama_guru", (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = n.input_id) AS "input_oleh" FROM tb_nilai as n LEFT JOIN tb_user as u ON n.siswa_id = u.id_user LEFT JOIN tb_tipe_nilai as tn ON n.tipe_id= tn.id_tipe_nilai LEFT JOIN tb_mapel as m ON n.mapel_id = m.id_mapel where m.guru_id='.$guru_id.'')->result();
        return $data;
        
    }

    public function getNilaiBySiswa()
    {
        $siswa_id = $this->session->userdata('id_user');
        $data = $this->db->query('SELECT n.id_nilai as "id_nilai", n.nilai as "nilai",u.id_user as "user_id", u.name_user,tn.id_tipe_nilai as "tipe_id",tn.nama_tipe_nilai,m.id_mapel as "mapel_id",m.nama_mapel, n.input_id, (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = m.guru_id) AS "nama_guru", (SELECT us.name_user FROM tb_user AS us WHERE us.id_user = n.input_id) AS "input_oleh" FROM tb_nilai as n LEFT JOIN tb_user as u ON n.siswa_id = u.id_user LEFT JOIN tb_tipe_nilai as tn ON n.tipe_id= tn.id_tipe_nilai LEFT JOIN tb_mapel as m ON n.mapel_id = m.id_mapel where n.siswa_id='.$siswa_id.'')->result();
        return $data;
        
    }

    public function cek(){
        $post = $this->input->post();
        $siswa_id = $post["siswa_id"];
        $mapel_id = $post["mapel_id"];
        $tipe_id = $post["tipe_id"];

        $data = $this->db->query('SELECT * FROM tb_nilai where siswa_id ='. $siswa_id.' and mapel_id = '.$mapel_id.' and tipe_id = '.$tipe_id.'');
        
        return $data;

    }

    public function save()
    {

        $post = $this->input->post();

        $this->siswa_id = $post["siswa_id"];
        $this->mapel_id = $post["mapel_id"];
        $this->tipe_id = $post["tipe_id"];
        $this->nilai = $post["nilai"];
        $this->input_id = $post["input_id"];

        return $this->db->insert($this->_table, $this);

    }

    public function update(){
        $post = $this->input->post();
        $this->id_nilai = $post["id_nilai"];
        $this->siswa_id = $post["siswa_id"];
        $this->mapel_id = $post["mapel_id"];
        $this->tipe_id = $post["tipe_id"];
        $this->nilai = $post["nilai"];
        $this->input_id = $post["input_id"];
        return $this->db->update($this->_table, $this, array('id_nilai' => $post['id_nilai']));
    }

    public function delete($id_nilai)
    {   
        return $this->db->delete($this->_table, array("id_nilai" => $id_nilai));
    }

}