<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kelas extends CI_Model
{
    private $_table = "tb_kelas";

    public $id_kelas;
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getKelas()
    {
         $this->db->select('*');
        $this->db->from('tb_kelas');
        $this->db->join('tb_user', 'tb_kelas.guru_id = tb_user.id_user');
        $this->db->where('tb_user.state =',"guru");
        // $query = $this->db->get();
        return $this->db->get()->result();
    }

    public function getMyKelas()
    {
        $id_user = $this->session->userdata('id_user');
        return $this->db->query('SELECT u.*,k.* FROM tb_user as u LEFT join tb_kelas as k ON u.kelas_id = k.id_kelas where k.guru_id = '.$id_user.' and u.state="siswa"')->result();
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
 
    public function save()
    {
 
        $post = $this->input->post();

        $this->name_kelas = $post["name_kelas"];
        $this->guru_id = $post["guru_id"];
        $this->tingkat = $post["tingkat"];
        $this->jurusan = $post["jurusan"];
        $this->is_active = 1;
        
        return $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id_kelas = $post["id_kelas"];
        $this->name_kelas = $post["name_kelas"];
        $this->guru_id = $post["guru_id"];
        $this->tingkat = $post["tingkat"];
        $this->jurusan = $post["jurusan"];
        $this->is_active = 1;
        return $this->db->update($this->_table, $this, array('id_kelas' => $post['id_kelas']));
    }

    public function delete($id_kelas)
    {   
        return $this->db->delete($this->_table, array("id_kelas" => $id_kelas));
    }

}